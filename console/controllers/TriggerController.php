<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.12.2019
 * Time: 17:11
 */

namespace console\controllers;

use common\models\TriggerEmailTrait;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class TriggerController extends Controller
{
    use TriggerEmailTrait;
    public function actionSendMessage(){

        $this->actionBirthday();
        $this->actionHoliday();
        $this->actionRemindBasket();
        $this->actionUpdatedTovarMonth();
    }

}
