<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;

class SignupForm extends Model
{

    public $email;
    public $password;
    public $username;
    public $password_verify;
    public $verifyCode;
    public $captcha;

    public function rules()
    {
        return [


            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass'=>'common\models\User'],

            [['username'], 'required'],
            [['username'], 'unique', 'targetClass'=>'common\models\User'],

            [['password'], 'required'],
            [['password_verify'], 'required'],
            [['password','password_verify'], 'string', 'min' => 6],
            [['password_verify'], 'compare', 'compareAttribute' => 'password', 'message' => 'В Поле "Подтверждение пароля", не соответствуют значения с полем "Пароль"'],

            [['verifyCode'],'required'],
            [['verifyCode'],'captcha'],


        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Почта',
            'password' => 'Пароль',
            'username' => 'Телефон',
            'verifyCode' => 'проверочный код',
            'password_verify' => 'Подтверждение пароля'
        ];
    }


    public function signUp()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->generateAuthKey();
        $user->setPassword($this->password);
        return $user->save(false) ? $user : null;
    }
}
