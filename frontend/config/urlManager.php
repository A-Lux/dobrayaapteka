<?php

/** @var array $params */

return [
        'class' => 'yii\web\UrlManager',
    //    'hostInfo' => 'http://azialife',
        'baseUrl' => '',
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'cache' => false,
        'rules' => [
            [
                'class' => 'yii\rest\UrlRule',
                'controller' => 'orders',
                'pluralize' => false,
            ],

            'catalog/filter' => 'catalog/filter',
            'catalog/offer-related-products' => 'catalog/offer-related-products',
            'catalog/filter-mob' => 'catalog/filter-mob',
            'catalog/load-more-products' => 'catalog/load-more-products',
            'catalog/load-more-products-mob' => 'catalog/load-more-products-mob',
            'catalog/load-more-button' => 'catalog/load-more-button',
            'catalog/dynamic-catalog' => 'catalog/dynamic-catalog',
            'catalog/load-more-button-mob' => 'catalog/load-more-button-mob',
            'product/<id:[\w-]+>/<url:[\w-]+>' => 'catalog/product',
            'product/<url:[\w-]+>' => 'catalog/product2',
            'catalog/<url:[\w-]+>' => 'catalog/index',
            'catalog/<url1:[\w-]+>/<url:[\w-]+>' => 'catalog/index',
            'catalog/<url1:[\w-]+>/<url2:[\w-]+>/<url:[\w-]+>' => 'catalog/index',
            'advantage/<url:[\w-]+>' => 'advantage/index',
            'feedback' => 'feedback/index',
            'sitemap' => 'site-map/index',
            'address' => 'address/index',
            'card' => 'card/index',
            'search' => 'search/index',
            'paybox/index' => 'paybox/index',
            'news' => 'news/index',
            'news/<url:[\w-]+>' => 'news/view',
            '<url:[\w-]+>' => 'site/index2',


            '<controller:\w+>/<id:\d+>'=>'<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
    ],
];
