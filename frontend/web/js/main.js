$(window).scroll(function () {
  var sticky = $(".hidden-head"),
    scroll = $(window).scrollTop();
  if (scroll >= 100) sticky.addClass("mobile-header");
  else sticky.removeClass("mobile-header");
});

$(window).scroll(function () {
  if ($(window).scrollTop() > 250) {
    $(".sticky-search").addClass("sticky-search-active");
  } else {
    $(".sticky-search").removeClass("sticky-search-active");
  }
});
$(window).scroll(function () {
  if ($(window).scrollTop() > 60) {
    $(".hc-nav-trigger").addClass("hc-nav-trigger-fixed");
    $(".hc-nav-trigger").css("margin-top", 8)
    $(".d-flex").addClass("width_animation")
    $(".mobile-flex").addClass("hc-nav-trigger-fixed");
    $(".search_btn_mob").css("right", 85)
  } else {
    $(".hc-nav-trigger").removeClass("hc-nav-trigger-fixed");
    $(".hc-nav-trigger").css("margin-top", 0)
    $(".d-flex").addClass("width_animation_0")
    $(".d-flex").removeClass("width_animation")
    $(".mobile-flex").removeClass("hc-nav-trigger-fixed");
    $(".search_btn_mob").css("right", 45)
  }
});

$(document).ready(function () {
  let owlItem = $(".owl-product > .owl-item");

  // $('<meta>', { name: 'viewport', content: 'user-scalable=no' }).appendTo('head');
  $("body").on("click", ".favorites", function () {
    var product_id = $(this).attr("data-id");
    $(this).toggleClass("favorites-active");
    $.ajax({
      url: "/account/add-to-favorite",
      data: { product_id: product_id },
      type: "GET",
      dataType: "json",
      success: function (data) {},
      error: function () {
        swal("Упс!", "Что-то пошло не так.", "error");
      },
    });
  });

  var maxField = 10; //Input fields increment limitation
  var addButton = $(".add_button"); //Add button selector
  var wrapper = $(".field_wrapper"); //Input field wrapper
  var fieldHTML = `<div class="personal-input">
    <div class="icon"><img src="images/loc-icon.png" alt=""></div>
    <input class="searcherrr" type="text" placeholder="Алматы, улица Зауыт, 1Б" name="UserAddress[address][]">
    <a href="javascript:void(0);" class="remove_button">
    <i class="fas fa-minus-circle"></i></a></div></div>`; //New input field html
  var x = 1; //Initial field counter is 1

  //Once add button is clicked
  $(addButton).click(function () {
    //Check maximum number of input fields
    if (x < maxField) {
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); //Add field html

      // $('input[name="UserAddress[address][]"]').inputmask({ mask: "Алматы, i{0,}",
      //   definitions: {
      //     "i": {
      //       validator: ".",
      //       definitionSymbol: "i"
      //     }
      //   },});

      $(`input[name="UserAddress[address][]"]`).autocomplete({
        source: function (request, response) {
          var address = request.term;
          var myGeocoder = ymaps.geocode("Алматы, " + address);
          myGeocoder.then(
            function (res) {
              var coords = res.geoObjects.get(0).geometry.getCoordinates();
              var myGeocoder = ymaps.geocode(coords, {
                kind: "house",
              });
              myGeocoder.then(function (res) {
                var myGeoObjects = res.geoObjects;
                var addresses = [];
                myGeoObjects.each(function (el, i) {
                  var arr = [];
                  arr["title"] = el.properties.get("name");
                  arr["value"] = "Алматы, " + el.properties.get("name");
                  arr["coords"] = el.geometry.getCoordinates();
                  addresses.push(arr);
                });

                response(
                  $.map(addresses, function (address) {
                    return {
                      label: address.title,
                      value: address.value,
                      coords: address.coords,
                    };
                  })
                );
              });
            },
            function (err) {
              alert("Ошибка");
            }
          );
        },
        minLength: 2,
        mustMatch: true,
      });
    }
  });

  //Once remove button is clicked
  $(wrapper).on("click", ".remove_button", function (e) {
    e.preventDefault();
    $(this).parent("div").remove(); //Remove field html
    x--; //Decrement field counter
  });
});



(function ($) {
  $('.rek_slider').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  })

  $("#city").click(function () {
    $(".pick-city-modal,.overlay").show();
  });
  $(".close-modal").click(function () {
    $(".pick-city-modal,.overlay").hide();
  });

  $(".gift-item").click(function () {
    $(this).find(".checked").toggle();
  });
  $(".instruction").click(function () {
    $(".rotate").toggle();
  });

  $(".modal-drug").hide();
  $(".catalog-menu li ").mouseenter(function () {
    $(this).find(".modal-drug").addClass("wow animated fadeIn").show(0);
  });
  $(".catalog-menu li ").mouseleave(function () {
    $(this).find(".modal-drug").hide();
  });

  $(".show-pass").click(function () {
    $(".show-pass").toggleClass("opacity-pass");
  });
  $("#main-nav").hcOffcanvasNav({
    maxWidth: 980,
  });

  $("#desktop-nav").hcOffcanvasNav({
    maxWidth: 2000,
  });

  // owl menu tabs

  // Change tab class and display content
  $(".tab-nav").click(function (e) {
    e.preventDefault();
    $(".tab-nav").removeClass("green");
    $(this).addClass("green");
  });
  $(".owl1 .item a").click(function (e) {
    // e.preventDefault();

    var href = $(this).attr("href");

    $(".tab-item2 ").removeClass("tab-item2-active ");
    $(href).addClass("tab-item2-active ");
  });
  $(".item a").click(function (e) {
    // e.preventDefault();

    var href = $(this).attr("href");

    $(".tab-item ").removeClass("tab-item-active ");
    $(href).addClass("tab-item-active ");
  });

  // end owl menu tabs
  let owl = $(".owl1");
  owl.owlCarousel({
    loop: true,
    // margin:10,
    nav: true,
    responsive: {
      0: {
        items: 2,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 5,
        nav: true,
      },
    },
  });

  $(".owl-arrow-right").click(function () {
    owl.trigger("next.owl.carousel");
  });
  owl.css("opacity", "1");
  $(".owl2").owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    responsive: {
      0: {
        items: 2,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 5,
        nav: true,
      },
    },
  });
$('.owl-main').owlCarousel({
    // center: true,
    items:1,
    stagePadding: 25,
    loop:true,
    margin: 5,
    responsive:{
        600:{
            items:2
        }
    }
});
$('.owl-main-mob').owlCarousel({
  items:1,
  stagePadding: 25,
  loop:true,
  margin: 5,
  responsive:{
      600:{
          items:1
      }
  }
});

})(jQuery);



function showPass() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPass2() {
  var x = document.getElementById("myInput2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPassm() {
  var x = document.getElementById("myInputm");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPassm2() {
  var x = document.getElementById("myInput2m");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

function locateToSignIn() {
  swal("Ошибка!", "Чтобы добавить товар в избранное нужно войти!", "error");
}

// When the user scrolls the page, execute myFunction
window.onscroll = function () {
  if (header) {
    myFunction();
  }
};

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
// Get the header
var header = document.getElementById("myHeader");

// Get the offset position of the navbar
if (header) {
  var sticky = header.offsetTop;
}

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
// function myFunction() {
//     if (window.pageYOffset > sticky) {
//         header.classList.add("sticky");
//     } else {
//         header.classList.remove("sticky");
//     }
// }
let fff = document.getElementsByClassName("menu-icon")[1];
if (fff) {
  fff.style.marginTop = "8px";
}

document.querySelectorAll(".nav-link").forEach((link) => {
  if (link.outerText.endsWith("Подарки")) {
    link.style.display = "none";
  }
});

tippy(".fa-heart", {
  trigger: "mouseenter",
  maxWidth: 200,
  arrow: false,
  content: "Добавить в избранное",
});


