<?php require_once 'header.php'?>
<div class="main">
   <div class="desktop-version">
       <div class="bg-white">
           <div class="container">
               <div class="row">
                   <div class="col-sm-12">
                       <div class="register-title text-center gradient-text wow fadeInDown">
                           <h5>Регистрация</h5>
                       </div>
                       <div class="register-by  gradient-text wow fadeInDown">
                           <p>Регистрация c помощью: <a href=""><img src="images/soc3.png" alt=""></a> <a href=""><img src="images/soc4.png" alt=""></a></p>
                       </div>
                   </div>
                   <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                       <form action="">
                           <div class="row">
                               <div class="col-sm-6">
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/user-icon-light.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Фамилия">
                                   </div>
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/user-icon-light.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Имя">
                                   </div>
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/user-icon-light.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Отчество">
                                   </div>
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/msg-icon.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Почта">
                                   </div>

                               </div>
                               <div class="col-sm-6">
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/phone-icon-light.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Номер  ">
                                   </div>
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/loc-icon.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Адрес">
                                   </div>
                                   <div class="field_wrapper">
                                       <div>

                                       </div>
                                       <div class="add-address gradient-text">
                                           <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-sm-6">
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/user-icon-light.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Логин">
                                   </div>
                                   <div class="capcha">
                                       <img src="images/capcha.jpg" alt="">

                                   </div>
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/lock-icon.png" alt="">
                                       </div>
                                       <input type="text" placeholder="Введите код с картинки">
                                   </div>
                                   <div class="review-btn">
                                       <button>Зарегистрироваться</button>
                                   </div>
                               </div>
                               <div class="col-sm-6">
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/lock-icon.png" alt="">
                                       </div>
                                       <input type="password" id="myInput" placeholder="Пароль">
                                       <button onclick="showPass()"><img src="images/show-icon.png" alt=""></button>
                                   </div>
                                   <div class="personal-input">
                                       <div class="icon">
                                           <img src="images/lock-icon.png" alt="">
                                       </div>
                                       <input type="password" id="myInput2" placeholder="Повторить пароль">
                                       <button onclick="showPass2()"><img src="images/show-icon.png" alt=""></button>
                                   </div>
                               </div>
                               <div class="col-sm-12">
                                   <div class="register-bonus">
                                       <p>Начисление бонусов при регистрации</p>
                                       <p><span class="gradient-text">500</span> тг!</p>
                                   </div>
                               </div>

                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
    <div class="mobile-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInDown">
                        <div class="register-title text-center gradient-text">
                            <h5>Регистрация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Регистрация c помощью:  </p>
                            <div class="row">
                                <div class="col left">
                                    <a href=""><img src="images/soc-lg.png" alt=""></a>
                                </div>
                                <div class="col right">
                                    <a href=""><img src="images/soc-lg2.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <form action="">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Фамилия">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Имя">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Отчество">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/msg-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Почта">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/phone-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Номер  ">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/loc-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Адрес">
                                    </div>
                                    <div class="field_wrapper">
                                        <div>

                                        </div>
                                        <div class="add-address gradient-text">
                                            <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Логин">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInputm" placeholder="Пароль">
                                        <button  onclick="showPassm()"><img src="images/show-icon.png" alt=""></button>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password"  id="myInput2m" placeholder="Повторить пароль">
                                        <button onclick="showPassm2()"><img src="images/show-icon.png" alt=""></button>
                                    </div>
                                    <div class="capcha">
                                        <img src="images/capcha.jpg" alt="">

                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="images/lock-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Введите код с картинки">
                                    </div>
                                    <div class="review-btn">
                                        <button>Зарегистрироваться</button>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="register-bonus">
                                        <p>Начисление бонусов при регистрации</p>
                                        <p><span class="gradient-text">500</span> тг!</p>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once 'footer.php'?>
