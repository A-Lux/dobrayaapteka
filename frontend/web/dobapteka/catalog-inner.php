<?php require_once 'header.php' ?>
<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInLeft">
                        <div class="catalog-menu">
                            <div class="title gradient-text">
                                Каталог товаров
                            </div>
                            <ul>
                                <li>
                                    <a href=""><span class="menu-icon"><img src="images/menu11.png" alt=""></span>
                                        <p>Лекарственные средства</p>
                                    </a>
                                    <div class="modal-drug">
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href=""><span class="menu-icon"><img src="images/menu21.png" alt=""></span>
                                        <p>Витамины и бады</p>
                                    </a>
                                    <div class="modal-drug">
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие2</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href=""><span class="menu-icon"><img src="images/menu31.png" alt=""></span>
                                        <p>Красота и гигиена</p>
                                    </a>
                                    <div class="modal-drug">
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие3</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href=""><span class="menu-icon"><img src="images/menu41.png" alt=""></span>
                                        <p>Мать и детя</p>
                                    </a>
                                    <div class="modal-drug">
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие4</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href=""><span class="menu-icon"><img src="images/menu51.png" alt=""></span>
                                        <p>Изделия мед. назначения</p>
                                    </a>
                                    <div class="modal-drug">
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие3</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                        <ul class="second-level-menu">
                                            <li><a href="">Жаропонижающие</a></li>
                                            <li><a href="">Пробиотики и пребиотики</a></li>
                                            <li><a href="">Противовирусные препараты</a></li>
                                            <li><a href="">Рецептурные препараты</a></li>
                                            <li><a href="">Слабительные препараты</a></li>
                                            <li><a href="">Спазмолитические препараты</a></li>
                                            <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tablet-version-hide">
                            <div class="main-title gradient-text">
                                <h5>Товар месяца</h5>
                            </div>
                            <div class="month-sale-item">
                                <a href="product-card.php">
                                    <div class="top">
                                        <div class="image">
                                            <img src="images/img1.png" alt="">
                                        </div>
                                        <div class="text-block">
                                            <div class="name">
                                                <p>Энтерожермина 5 мл</p>
                                            </div>
                                            <div class="price">
                                                <p>1900</p> <span class="gradient-text">1200</span>тг
                                            </div>
                                            <div class="status">
                                                <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bot">
                                        <div class="sale">
                                            <p>40%</p>
                                            <div class="bonus">
                                                20 Бонус
                                            </div>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="month-sale-item">
                                <a href="product-card.php">
                                    <div class="top">
                                        <div class="image">
                                            <img src="images/img1.png" alt="">
                                        </div>
                                        <div class="text-block">
                                            <div class="name">
                                                <p>Энтерожермина 5 мл</p>
                                            </div>
                                            <div class="price">
                                                <p>1900</p> <span class="gradient-text">1200</span>тг
                                            </div>
                                            <div class="status">
                                                <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bot">
                                        <div class="sale">
                                            <p>40%</p>
                                            <div class="bonus">
                                                20 Бонус
                                            </div>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-6 col-sm-12">
                        <div class="white-bg wow fadeInRight">
                            <div class="title gradient-text">
                                <h6>Антисептики</h6>
                            </div>
                            <div class="row-list">
                                <div class="back-link">
                                    <a href=""><span>&#8592</span>Назад к Лекарственные средства</a>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-version-hide filter wow fadeInUp">
                            <div class="row">
                                <!-- <div class="col-sm-3 border-right"> -->
                                    <!--                                    <div class="select">-->
                                    <!--                                        <select name="" id="">Сначала новые-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                        </select>-->
                                    <!--                                    </div>-->
                                <!-- </div> -->
                                <div class="col-sm-6  border-right">
                                    <div class="filter-form">
                                        <label for="from">Цена от:</label>
                                        <input type="text" id="from">
                                        <label for="to">Цена до:</label>
                                        <input type="text" id="to">
                                        <label for="to">тг</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox-flex">
                                        <label class="filter-checkbox">В наличии
                                            <input type="checkbox" checked="checked">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-version-hide row wow fadeInUp">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="link-wrap">
                                    <a href="product-card.php">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img5.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <!-- <div class="red-label">
                                                    Дефицит
                                                </div> -->
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="link-wrap">
                                    <a href="">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img6.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="link-wrap">
                                    <a href="product-card.php">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img4.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <!-- <div class="red-label">
                                                    Дефицит
                                                </div> -->
                                                <div class="name">
                                                    <p>Аллохол №50, табл., покрытые оболочкой</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="link-wrap">
                                    <a href="product-card.php">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <!-- <div class="red-label">
                                                    Дефицит
                                                </div> -->
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="link-wrap">
                                    <a href="product-card.php">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="link-wrap">
                                    <a href="product-card.php">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div id="demo" class="collapse">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="link-wrap">
                                                <a href="product-card.php">
                                                    <div class="catalog-item">
                                                        <div class="image">
                                                            <img src="images/img1.png" alt="">
                                                        </div>
                                                        <div class="text-block">
                                                            <div class="red-label">
                                                                Дефицит
                                                            </div>
                                                            <div class="name">
                                                                <p>Йодомарин 10 табл.</p>
                                                            </div>
                                                            <div class="status">
                                                                <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                            </div>
                                                            <div class="flex-one">
                                                                <div class="price">
                                                                    <p class="gradient-text">369 <span> тг.</span></p>
                                                                </div>
                                                                <div class="catalog-basket-button">
                                                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="link-wrap">
                                                <a href="product-card.php">
                                                    <div class="catalog-item">
                                                        <div class="image">
                                                            <img src="images/img1.png" alt="">
                                                        </div>
                                                        <div class="text-block">
                                                            <div class="red-label">
                                                                Дефицит
                                                            </div>
                                                            <div class="name">
                                                                <p>Йодомарин 10 табл.</p>
                                                            </div>
                                                            <div class="status">
                                                                <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                            </div>
                                                            <div class="flex-one">
                                                                <div class="price">
                                                                    <p class="gradient-text">369 <span> тг.</span></p>
                                                                </div>
                                                                <div class="catalog-basket-button">
                                                                    <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="load-more">
                                    <button data-toggle="collapse" data-target="#demo">Загрузить еще</button>
                                </div>

                            </div>

                            <div class="col-sm-12">
                                <div class="catalog-title gradient-text text-center">
                                    <h6>Бонусы и подарки</h6>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="white-wrap">
                                    <img src="images/advantage-icon4.png" alt="">
                                    <p>Получайте бонусы за каждую покупку и в любой момент обменивайте их на подарки!</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="catalog-bonus-list">
                                    <ul>
                                        <li>Обменивайте бонусы на скидки до 99% при покупке купонов и сертификатов;</li>
                                        <li>1 бонус = 1 тенге скидки;</li>
                                        <li>Получайте дополнительно до 20% за покупки купонов и сертификатов;</li>
                                        <li><a href="" style="font-size: 15px!important" class="gradient-text">Авторизуйтесь</a> на сайте, чтобы покупать быстрее и проще;</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tablet-version col-sm-12">
                        <div class="main-title gradient-text">
                            <h5>Товар месяца</h5>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="month-sale-item">
                                    <a href="product-card.php">
                                        <div class="top">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <p>Энтерожермина 5 мл</p>
                                                </div>
                                                <div class="price">
                                                    <p>1900 <span class="gradient-text">1200</span>тг</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bot">
                                            <div class="sale">
                                                <p>40%</p>
                                            </div>
                                            <div class="sale-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="month-sale-item">
                                    <a href="product-card.php">
                                        <div class="top">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <p>Энтерожермина 5 мл</p>
                                                </div>
                                                <div class="price">
                                                    <p>1900 <span class="gradient-text">1200</span>тг</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bot">
                                            <div class="sale">
                                                <p>40%</p>
                                            </div>
                                            <div class="sale-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="filter wow fadeInUp">
                            <div class="row">
                                <div class="col-sm-3 border-right">
                                    <!--                                    <div class="select">-->
                                    <!--                                        <select name="" id="">Сначала новые-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                            <option value="">Сначала новые </option>-->
                                    <!--                                        </select>-->
                                    <!--                                    </div>-->
                                </div>
                                <div class="col-sm-6 border-right">
                                    <div class="filter-form">
                                        <label for="from">Цена от:</label>
                                        <input type="text" id="from">
                                        <label for="to">Цена до:</label>
                                        <input type="text" id="to">
                                        <label for="to">тг</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox-flex">
                                        <label class="filter-checkbox">В наличии
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="images/img1.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <div class="red-label">
                                            Дефицит
                                        </div>
                                        <div class="name">
                                            <p>Йодомарин 10 табл.</p>
                                        </div>
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="flex-one">
                                            <div class="price">
                                                <p class="gradient-text">369 <span> тг.</span></p>
                                            </div>
                                            <div class="catalog-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="images/img3.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <div class="name">
                                            <p>Йодомарин 10 табл.</p>
                                        </div>
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="flex-one">
                                            <div class="price">
                                                <p class="gradient-text">369 <span> тг.</span></p>
                                            </div>
                                            <div class="catalog-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="images/img4.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <div class="red-label">
                                            Дефицит
                                        </div>
                                        <div class="name">
                                            <p>Аллохол №50, табл., покрытые оболочкой</p>
                                        </div>
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="flex-one">
                                            <div class="price">
                                                <p class="gradient-text">369 <span> тг.</span></p>
                                            </div>
                                            <div class="catalog-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="images/img1.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <div class="red-label">
                                            Дефицит
                                        </div>
                                        <div class="name">
                                            <p>Йодомарин 10 табл.</p>
                                        </div>
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="flex-one">
                                            <div class="price">
                                                <p class="gradient-text">369 <span> тг.</span></p>
                                            </div>
                                            <div class="catalog-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="images/img1.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <div class="red-label">
                                            Дефицит
                                        </div>
                                        <div class="name">
                                            <p>Йодомарин 10 табл.</p>
                                        </div>
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="flex-one">
                                            <div class="price">
                                                <p class="gradient-text">369 <span> тг.</span></p>
                                            </div>
                                            <div class="catalog-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="images/img1.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <div class="red-label">
                                            Дефицит
                                        </div>
                                        <div class="name">
                                            <p>Йодомарин 10 табл.</p>
                                        </div>
                                        <div class="status">
                                            <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                        </div>
                                        <div class="flex-one">
                                            <div class="price">
                                                <p class="gradient-text">369 <span> тг.</span></p>
                                            </div>
                                            <div class="catalog-basket-button">
                                                <button><img src="images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="collapse-wrap">
                                    <div id="demo2" class="collapse">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="link-wrap">
                                                    <a href="product-card.php">
                                                        <div class="catalog-item">
                                                            <div class="image">
                                                                <img src="images/img1.png" alt="">
                                                            </div>
                                                            <div class="text-block">
                                                                <div class="red-label">
                                                                    Дефицит
                                                                </div>
                                                                <div class="name">
                                                                    <p>Йодомарин 10 табл.</p>
                                                                </div>
                                                                <div class="status">
                                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                </div>
                                                                <div class="flex-one">
                                                                    <div class="price">
                                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                                    </div>
                                                                    <div class="catalog-basket-button">
                                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="link-wrap">
                                                    <a href="product-card.php">
                                                        <div class="catalog-item">
                                                            <div class="image">
                                                                <img src="images/img1.png" alt="">
                                                            </div>
                                                            <div class="text-block">
                                                                <div class="red-label">
                                                                    Дефицит
                                                                </div>
                                                                <div class="name">
                                                                    <p>Йодомарин 10 табл.</p>
                                                                </div>
                                                                <div class="status">
                                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                </div>
                                                                <div class="flex-one">
                                                                    <div class="price">
                                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                                    </div>
                                                                    <div class="catalog-basket-button">
                                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="load-more">
                                        <button data-toggle="collapse" data-target="#demo2">Загрузить еще</button>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="catalog-title gradient-text text-center">
                                    <h6>Бонусы и подарки</h6>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="white-wrap">
                                    <img src="images/advantage-icon4.png" alt="">
                                    <p>Получайте бонусы за каждую покупку и в любой момент обменивайте их на подарки!</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="catalog-bonus-list">
                                    <ul>
                                        <li>Обменивайте бонусы на скидки до 99% при покупке купонов и сертификатов;</li>
                                        <li>1 бонус = 1 тенге скидки;</li>
                                        <li>Получайте дополнительно до 20% за покупки купонов и сертификатов;</li>
                                        <li><a href="" style="font-size: 15px!important" class="gradient-text">Авторизуйтесь</a> на сайте, чтобы покупать быстрее и проще;</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="owl1 owl-carousel owl-theme">
                            <div class="item">
                                <div class="green">
                                    <a href="#tab">Лекарственные средства</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="green">
                                    <a href="#tab2">Витамины и бады</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="green">
                                    <a href="#tab3">Лекарственные средства</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="green">
                                    <a href="#tab4">Лекарственные средства</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="green">
                                    <a href="#tab5">Лекарственные средства</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="filter">
                            <div class="row">
                                <div class="col-sm-3 ">
                                    <select id="selectbox1">
                                        <option value="ala">Сначала новые</option>
                                        <option value="nur">Нур-Султан</option>
                                        <option value="pavlo">Павлодар</option>
                                        <option value="shym">Шымкент</option>
                                        <option value="akt">Актау</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 ">
                                    <div class="filter-form">
                                        <label for="from">Цена от:</label>
                                        <input type="text" id="from">
                                        <label for="to">Цена до:</label>
                                        <input type="text" id="to">
                                        <label for="to">тг</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 ">
                                    <div class="checkbox-flex">
                                        <!-- <label class="filter-checkbox">В наличии
                                            <input type="checkbox" checked="checked">
                                            <span class="checkmark"></span>
                                        </label> -->
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-item tab-item-active" id="home">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img3.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img4.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Аллохол №50, табл., покрытые оболочкой</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div id="demo2" class="collapse">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="link-wrap">
                                                        <a href="product-card.php">
                                                            <div class="catalog-item">
                                                                <div class="image">
                                                                    <img src="images/img1.png" alt="">
                                                                </div>
                                                                <div class="text-block">
                                                                    <div class="red-label">
                                                                        Дефицит
                                                                    </div>
                                                                    <div class="name">
                                                                        <p>Йодомарин 10 табл.</p>
                                                                    </div>
                                                                    <div class="status">
                                                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                    </div>
                                                                    <div class="flex-one">
                                                                        <div class="price">
                                                                            <p class="gradient-text">369 <span> тг.</span></p>
                                                                        </div>
                                                                        <div class="catalog-basket-button">
                                                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="link-wrap">
                                                        <a href="product-card.php">
                                                            <div class="catalog-item">
                                                                <div class="image">
                                                                    <img src="images/img1.png" alt="">
                                                                </div>
                                                                <div class="text-block">
                                                                    <div class="red-label">
                                                                        Дефицит
                                                                    </div>
                                                                    <div class="name">
                                                                        <p>Йодомарин 10 табл.</p>
                                                                    </div>
                                                                    <div class="status">
                                                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                    </div>
                                                                    <div class="flex-one">
                                                                        <div class="price">
                                                                            <p class="gradient-text">369 <span> тг.</span></p>
                                                                        </div>
                                                                        <div class="catalog-basket-button">
                                                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="load-more">
                                            <button data-toggle="collapse" data-target="#demo2">Загрузить еще</button>
                                        </div>
                                        <div class="catalog-title gradient-text text-center">
                                            <h6>Бонусы и подарки</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="white-wrap">
                                            <img src="images/advantage-icon4.png" alt="">
                                            <p>Получайте бонусы за каждую покупку и в любой момент обменивайте их на подарки!</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="catalog-bonus-list">
                                            <ul>
                                                <li>Обменивайте бонусы на скидки до 99% при покупке купонов и сертификатов;</li>
                                                <li>1 бонус = 1 тенге скидки;</li>
                                                <li>Получайте дополнительно до 20% за покупки купонов и сертификатов;</li>
                                                <li><a href="" style="font-size: 15px!important" class="gradient-text">Авторизуйтесь</a> на сайте, чтобы покупать быстрее и проще;</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-item" id="home2">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Йодомаринssss 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img3.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img4.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Аллохол №50, табл., покрытые оболочкой</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div id="demo2" class="collapse">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="link-wrap">
                                                        <a href="product-card.php">
                                                            <div class="catalog-item">
                                                                <div class="image">
                                                                    <img src="images/img1.png" alt="">
                                                                </div>
                                                                <div class="text-block">
                                                                    <div class="red-label">
                                                                        Дефицит
                                                                    </div>
                                                                    <div class="name">
                                                                        <p>Йодомарин 10 табл.</p>
                                                                    </div>
                                                                    <div class="status">
                                                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                    </div>
                                                                    <div class="flex-one">
                                                                        <div class="price">
                                                                            <p class="gradient-text">369 <span> тг.</span></p>
                                                                        </div>
                                                                        <div class="catalog-basket-button">
                                                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="link-wrap">
                                                        <a href="product-card.php">
                                                            <div class="catalog-item">
                                                                <div class="image">
                                                                    <img src="images/img1.png" alt="">
                                                                </div>
                                                                <div class="text-block">
                                                                    <div class="red-label">
                                                                        Дефицит
                                                                    </div>
                                                                    <div class="name">
                                                                        <p>Йодомарин 10 табл.</p>
                                                                    </div>
                                                                    <div class="status">
                                                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                    </div>
                                                                    <div class="flex-one">
                                                                        <div class="price">
                                                                            <p class="gradient-text">369 <span> тг.</span></p>
                                                                        </div>
                                                                        <div class="catalog-basket-button">
                                                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="load-more">
                                            <button data-toggle="collapse" data-target="#demo2">Загрузить еще</button>
                                        </div>

                                        <div class="catalog-title gradient-text text-center">
                                            <h6>Бонусы и подарки</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="white-wrap">
                                            <img src="images/advantage-icon4.png" alt="">
                                            <p>Получайте бонусы за каждую покупку и в любой момент обменивайте их на подарки!</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="catalog-bonus-list">
                                            <ul>
                                                <li>Обменивайте бонусы на скидки до 99% при покупке купонов и сертификатов;</li>
                                                <li>1 бонус = 1 тенге скидки;</li>
                                                <li>Получайте дополнительно до 20% за покупки купонов и сертификатов;</li>
                                                <li><a href="" style="font-size: 15px!important" class="gradient-text">Авторизуйтесь</a> на сайте, чтобы покупать быстрее и проще;</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-item" id="home3">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img1.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Йодомаринssss 103 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img3.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <p>Йодомарин 10 табл.</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <img src="images/img4.png" alt="">
                                            </div>
                                            <div class="text-block">
                                                <div class="red-label">
                                                    Дефицит
                                                </div>
                                                <div class="name">
                                                    <p>Аллохол №50, табл., покрытые оболочкой</p>
                                                </div>
                                                <div class="status">
                                                    <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text">369 <span> тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div id="demo2" class="collapse">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="link-wrap">
                                                        <a href="product-card.php">
                                                            <div class="catalog-item">
                                                                <div class="image">
                                                                    <img src="images/img1.png" alt="">
                                                                </div>
                                                                <div class="text-block">
                                                                    <div class="red-label">
                                                                        Дефицит
                                                                    </div>
                                                                    <div class="name">
                                                                        <p>Йодомарин 10 табл.</p>
                                                                    </div>
                                                                    <div class="status">
                                                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                    </div>
                                                                    <div class="flex-one">
                                                                        <div class="price">
                                                                            <p class="gradient-text">369 <span> тг.</span></p>
                                                                        </div>
                                                                        <div class="catalog-basket-button">
                                                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="link-wrap">
                                                        <a href="product-card.php">
                                                            <div class="catalog-item">
                                                                <div class="image">
                                                                    <img src="images/img1.png" alt="">
                                                                </div>
                                                                <div class="text-block">
                                                                    <div class="red-label">
                                                                        Дефицит
                                                                    </div>
                                                                    <div class="name">
                                                                        <p>Йодомарин 10 табл.</p>
                                                                    </div>
                                                                    <div class="status">
                                                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                                                    </div>
                                                                    <div class="flex-one">
                                                                        <div class="price">
                                                                            <p class="gradient-text">369 <span> тг.</span></p>
                                                                        </div>
                                                                        <div class="catalog-basket-button">
                                                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="load-more">
                                            <button data-toggle="collapse" data-target="#demo2">Загрузить еще</button>
                                        </div>

                                        <div class="catalog-title gradient-text text-center">
                                            <h6>Бонусы и подарки</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="white-wrap">
                                            <img src="images/advantage-icon4.png" alt="">
                                            <p>Получайте бонусы за каждую покупку и в любой момент обменивайте их на подарки!</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="catalog-bonus-list">
                                            <ul>
                                                <li>Обменивайте бонусы на скидки до 99% при покупке купонов и сертификатов;</li>
                                                <li>1 бонус = 1 тенге скидки;</li>
                                                <li>Получайте дополнительно до 20% за покупки купонов и сертификатов;</li>
                                                <li><a href="" style="font-size: 15px!important" class="gradient-text">Авторизуйтесь</a> на сайте, чтобы покупать быстрее и проще;</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once 'footer.php' ?>