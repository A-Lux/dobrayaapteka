<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Добрая аптека</title>    
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/smoothbox.css">
    <link rel="stylesheet" href="css/hc-offcanvas-nav.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" >
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery.maskedinput.js"></script>
</head>
<body>
<div class="overlay">
    <div class="pick-city-modal">
        <div class="close-modal">
            &#10005
        </div>
        <h2>Выберите ваш город</h2>
        <div class="city-list">
            <ul>
                <li><a href="">Алматы</a></li>
                <li><a href="">Нур-Султан</a></li>
                <li><a href="">Актобе</a></li>
                <li><a href="">Актау</a></li>
            </ul>
            <ul>
                <li><a href="">Талдыкорган</a></li>
                <li><a href="">Тараз</a></li>
                <li><a href="">Шымкент</a></li>
            </ul>
            <ul>
                <li><a href="">Усть-Каменогорск</a></li>
                <li><a href="">Караганда</a></li>
                <li><a href="">Кызылорда</a></li>
                <li><a href="">Уральск</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="content">
   <div class="desktop-version">
       <header class="header" >
           <div class="container">
               <div class="row">
                   <div class="col-sm-2">
                       <div class="logo wow fadeInLeft">
                           <a href="index.php">
                               <img src="images/logo.png" alt="">
                           </a>
                       </div>
                   </div>
                   <div class="col-sm-2 ">
                       <div class="loc">
                           <div class="loc-icon">
                               <img src="images/loc-icon.png" alt="">
                           </div>
                           <span>Алматы <img src="images/arrow.png" alt=""></span>
                       </div>
                   </div>
                   <div class="col-sm-8 ">
                       <div class="menu wow fadeInDown">
                           <ul>
                               <li><a href="">О компании</a></li>
                               <li><a href="">Отзывы</a></li>
                               <li><a href="">Адреса аптек</a></li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
           <div  id="myHeader">
               <div class="container">
                   <div class="row wow fadeInUp">
                       <div class="col-sm-6">
                           <div class="search">
                               <input type="text" placeholder="Введите название товара">
                           </div>
                       </div>
                       <div class="offset-lg-2 col-sm-3 col-md-3 col-lg-2">
                           <div class="loc">
                               <div class="loc-icon">
                                   <img src="images/phone-icon.png" alt="">
                               </div>
                               <a href="tel:+77773525545"><p>+7 777 352 55 45</p></a>
                           </div>
                       </div>
                       <div class="col-sm-1">
                           <div class="user-icon">
                               <a href="tabs.php">
                                   <img src="images/user-icon.png" alt="">
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-1">
                           <div class="basket-icon">
                               <a href="basket.php">
                                   <img src="images/basket-icon.png" alt="">
                                   <span class="count">1</span>
                               </a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

       </header>
   </div>
    <div class="mobile-version ">
        <header>
            <div class="container">
                <div class="row wow fadeInDown">
                    <div class="col" style="display: flex">
                        <nav id="main-nav">
                            <ul>
                                <li><a href="#">Каталог</a>
                                    <ul>
                                        <li><a href="#"><span class="menu-icon"><img src="images/menu1.png" alt=""></span><p>Лекарственные средства</p></a>
                                            <ul>
                                                <li><a href="">Жаропонижающие</a></li>
                                                <li><a href="">Пробиотики и пребиотики</a></li>
                                                <li><a href="">Противовирусные препараты</a></li>
                                                <li><a href="">Рецептурные препараты</a></li>
                                                <li><a href="">Слабительные препараты</a></li>
                                                <li><a href="">Спазмолитические препараты</a></li>
                                                <li> <a href="#"><span class="menu-icon"><img src="images/menu2.png" alt=""></span><p>Витамины и бады</p></a>
                                                    <ul>
                                                        <li><a href="">Жаропонижающие</a></li>
                                                        <li><a href="">Пробиотики и пребиотики</a></li>
                                                        <li><a href="">Противовирусные препараты</a></li>
                                                        <li><a href="">Рецептурные препараты</a></li>
                                                        <li><a href="">Слабительные препараты</a></li>
                                                        <li><a href="">Спазмолитические препараты</a></li>
                                                        <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li> <a href="#"><span class="menu-icon"><img src="images/menu2.png" alt=""></span><p>Витамины и бады</p></a>
                                            <ul>
                                                <li><a href="">Жаропонижающие</a></li>
                                                <li><a href="">Пробиотики и пребиотики</a></li>
                                                <li><a href="">Противовирусные препараты</a></li>
                                                <li><a href="">Рецептурные препараты</a></li>
                                                <li><a href="">Слабительные препараты</a></li>
                                                <li><a href="">Спазмолитические препараты</a></li>
                                                <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#"><span class="menu-icon"><img src="images/menu3.png" alt=""></span><p>Красота и гигиена</p></a>

                                        </li>
                                        <li><a href="#"><span class="menu-icon"><img src="images/menu4.png" alt=""></span><p>Мать и детя</p></a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><i class="fas fa-building"></i>О компании</a></li>
                                <li><a href="#"><i class="fas fa-comment-dots"></i>Отзывы</a></li>
                                <li><a href="#"><i class="fas fa-map-marked-alt"></i>Адреса аптек</a>
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">1</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <div class="mobile-select">
                            <div class="loc">
                                <div class="loc-icon">
                                    <img src="images/loc-icon.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="logo">
                            <a href="index.php">
                                <img src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mobile-flex">
                            <div class="basket-icon">
                                <a href="basket.php">
                                    <img src="images/basket-icon.png" alt="">
                                    <span class="count">1</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="search">
                            <input type="text" placeholder="Введите название товара">
                        </div>
                    </div>

                </div>
            </div>
            <div class="hidden-head ">
                <div class="container">
                    <div class="row wow fadeInDown">
                        <div class="col">
                            <nav id="main-nav">
                                <ul>
                                    <li><a href="#">Каталог</a>
                                        <ul>
                                            <li><a href="#"><span class="menu-icon"><img src="images/menu1.png" alt=""></span><p>Лекарственные средства</p></a>
                                                <ul>
                                                    <li><a href="">Жаропонижающие</a></li>
                                                    <li><a href="">Пробиотики и пребиотики</a></li>
                                                    <li><a href="">Противовирусные препараты</a></li>
                                                    <li><a href="">Рецептурные препараты</a></li>
                                                    <li><a href="">Слабительные препараты</a></li>
                                                    <li><a href="">Спазмолитические препараты</a></li>
                                                    <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                                </ul>
                                            </li>
                                            <li> <a href="#"><span class="menu-icon"><img src="images/menu2.png" alt=""></span><p>Витамины и бады</p></a>
                                                <ul>
                                                    <li><a href="">Жаропонижающие</a></li>
                                                    <li><a href="">Пробиотики и пребиотики</a></li>
                                                    <li><a href="">Противовирусные препараты</a></li>
                                                    <li><a href="">Рецептурные препараты</a></li>
                                                    <li><a href="">Слабительные препараты</a></li>
                                                    <li><a href="">Спазмолитические препараты</a></li>
                                                    <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#"><span class="menu-icon"><img src="images/menu3.png" alt=""></span><p>Красота и гигиена</p></a>

                                            </li>
                                            <li><a href="#"><span class="menu-icon"><img src="images/menu4.png" alt=""></span><p>Мать и детя</p></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><i class="fas fa-building"></i>О компании</a></li>
                                    <li><a href="#"><i class="fas fa-comment-dots"></i>Отзывы</a></li>
                                    <li><a href="#"><i class="fas fa-map-marked-alt"></i>Адреса аптек</a>
                                        <ul>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">1</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-8">
                            <div class="head-search">
                                <input type="text" placeholder="Поиск по сайту">
                            </div>
                        </div>
                        <div class="col">
                            <div class="mobile-flex">
                                <div class="head-basket-icon">
                                    <a href="basket.php">
                                        <img src="images/basket-icon.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
    </div>
