<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.08.2019
 * Time: 16:19
 */

namespace frontend\controllers;

use common\models\Products;
use common\models\SearchBanner;
use yii\data\Pagination;
use frontend\models\ProductSearch;

class SearchController extends FrontendController
{
    function wordcombos ($words) {
        if ( count($words) <= 1 ) { 
            $result=$words; 
        } 
        else { 
            $result=array(); 
            for ( $i=0; $i < count($words); ++$i ) {
                $firstword=$words[$i]; $remainingwords=array(); 
                for ( $j=0; $j < count($words); ++$j ) { 
                    if ( $i <> $j )
                        $remainingwords[] = $words[$j];
                }
                $combos = $this->wordcombos($remainingwords);
                for ( $j = 0; $j < count($combos); ++$j ) { 
                    $result[]=$firstword . '%' . $combos[$j]; 
                } 
            } 
        } 
        
        return $result; 
    }


    public function actionIndex($text)
    {
        // if (apc_exists($text . 'newSearch')) {
        //     return apc_fetch($text . 'newSearch');
        // }
        
        $final = 'name like \'%';
        if(count(explode(' ', $text)) <= 3){
            foreach ($this->wordcombos(explode(' ', $text)) as $words) {
                $final = $final . $words .'%\' or name like \'%';
            }
            $final = preg_replace('/ or name like \'%$/', '', $final);
        }
        else{
            $final = $final . str_replace(' ', '%', $text) . '%\'';
        }
        
        $keyword = $text;
        // str_replace(' ', '%', $text);

        if(isMobile()){
            $pageSize = 20;
        }else{
            if(!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)){
                $pageSize = 14;
            }else{
                $pageSize = 16;
            }

        }

        $count = ProductSearch::find()
            // ->andFilterWhere(['like', 'name', $keyword])
            ->where($final)
            ->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => $pageSize]);
        $product = ProductSearch::find()
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->where($final)
            ->all();

        $keyword = str_replace('%', ' ', $text);

        // apc_store($text . 'newSearch', $product, 1800);


        $banner = SearchBanner::getContent();
        $this->view->registerMetaTag(['name' => 'robots', 'content' => "noindex, nofollow"]);

        return $this->render('index', compact('keyword', 'count', 'product', 'pagination', 'banner', 'pageSize'));

    }
}