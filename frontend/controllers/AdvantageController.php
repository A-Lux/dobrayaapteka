<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.12.2019
 * Time: 12:25
 */

namespace frontend\controllers;


use common\models\Advantages;

class AdvantageController extends FrontendController
{
    public function actionIndex($url){

        $model = Advantages::findOne(['url'=>$url]);
        return $this->render('index',compact('model'));
    }

}
