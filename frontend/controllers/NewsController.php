<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.2020
 * Time: 15:46
 */

namespace frontend\controllers;


use common\models\FilialProduct;
use common\models\News;
use common\models\Products;
use frontend\models\ProductSearch;
use yii\web\NotFoundHttpException;

class NewsController extends FrontendController
{

    public function actionIndex(){

        $model = News::getAll();
        return $this->render('index', compact('model'));
    }


    public function actionView($url){

        $model = News::findOne(['url' => $url]);
        if($model){

            $ids = [];
            preg_match_all('/{(.*?)}/', $model->full_description, $matches);
            
//            $model->full_description = preg_replace('{\d+}', '', $model->full_description);


            for ($i = 0; $i < count($matches[0]); $i++){
                $ids[] = (int) filter_var($matches[0][$i], FILTER_SANITIZE_NUMBER_INT);
            }

            $data = $this->getData($ids);
            foreach ($data as $product){
//                print_r($product);

                $model->full_description = preg_replace('/{(.*?)}/',$product,$model->full_description,1);
            }
            $model->full_description = str_replace('13269','',$model->full_description);
            $model->full_description = str_replace('{', '', $model->full_description);
            $model->full_description = str_replace('}', '', $model->full_description);
            return $this->render('inner', compact('model', 'data'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    protected function getData($ids){

        $products = Products::find()
            ->where(['in', 'id', $ids])
            ->all();

        $data = [];
        foreach ($products as $v){
            $check = FilialProduct::find()
                ->distinct()
                ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                ->where('filial.status = 2')
                ->andWhere(['filial_product.product_id' => $v->id])
                ->andWhere('filial_product.amount > 0')
                ->exists();

            if($check){

                if(!isMobile()){
                    $product = $this->renderPartial('/partials/desktop_product', ['product' => $v, 'class' => 'col-lg-3 col-md-6 col-sm-12 wow fadeInUp']);
                }else {
                    $product = $this->renderPartial('/partials/mobile_product', ['product' => $v, 'class' => 'col-lg-3 col-md-6 col-sm-12 col-6 mob_p wow fadeInUp']);
                }

                $data[] = $product;
            }else{
                $data[] = '';
            }


        }

        return $data;
    }

}
