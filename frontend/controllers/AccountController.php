<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2019
 * Time: 11:31
 */

namespace frontend\controllers;

use app\models\Auth;
use common\models\Message;
use common\models\Orders;
use common\models\Products;
use common\models\UserBalance;
use common\models\UserFavorites;
use common\models\UserGift;
use frontend\models\LoginForm;
use common\models\User;
use common\models\UserAddress;
use common\models\UserProfile;


use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use Yii;
use yii\debug\models\search\Profile;
use yii\web\NotFoundHttpException;


class AccountController extends FrontendController
{

    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        $attributes = $client->getUserAttributes();

//        echo '<pre>'; var_dump($attributes);die;
        /* @var $auth Auth */
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();


        if (Yii::$app->user->isGuest) {
            if ($auth) { // авторизация
                $user = $auth->user;
                Yii::$app->user->login($user);
                return $this->redirect('/account/?tab=home');
            } else { // регистрация
                if (isset($attributes['email']) && User::find()->where(['email' => $attributes['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('facebook_error', "Пользователь с такой электронной почтой как ".$attributes['email']." уже существует!");
                    return $this->redirect('/account/sign-in');
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    $user = new User();
                    if(isset( $attributes['email'])) {
                        $user->email = $attributes['email'];
                    }

                    $user->generateAuthKey();
                    $user->setPassword($password);
                    $user->generatePasswordResetToken();
                    $transaction = $user->getDb()->beginTransaction();
                    if ($user->save(false)) {
                        if($client->getId() == "google"){
                            $profile = new UserProfile([
                                'user_id' => $user->id,
                                'fio' => (string)$attributes["given_name"].' '.(string)$attributes["family_name"],
                            ]);
                        }else{
                            $fio = explode(" ",$attributes["name"]);
                            $profile = new UserProfile([
                                'user_id' => $user->id,
                                'fio' => (string)$fio[0].' '.(string)$fio[1],
                            ]);
                        }

                        if ($profile->save()) {
                            $auth = new Auth([
                                'user_id' => $user->id,
                                'source' => $client->getId(),
                                'source_id' => (string)$attributes['id'],
                            ]);

                            $address = new UserAddress([
                                'user_id' => $user->id,
                                'address' => "Укажите ваш адрес",
                            ]);
                            if ($address->save(false)) {
                                if ($auth->save()) {
                                    UserBalance::createUserBalanceForRegistration($user->id);
                                    $transaction->commit();
                                    $_SESSION['user_id'] = $user->id;
                                    $_SESSION['user_password'] = $password;
                                    return $this->redirect('/account/confirm-telephone');
                                } else {
                                    print_r($auth->getErrors());
                                    die;
                                }
                            }else {
                                print_r($address->getErrors());
                                die;
                            }
                        } else {
                            print_r($profile->getErrors());
                            die;
                        }
                    } else {
                        print_r($user->getErrors());
                        die;
                    }
                }
            }
        } else { // Пользователь уже зарегистрирован
            if (!$auth) { // добавляем внешний сервис аутентификации
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();
            }
        }
    }



    public function actionConfirmTelephone(){
        return $this->render('confirm-telephone');
    }

    public function actionUpdatePhone(){

        if (Yii::$app->request->isAjax) {
            $user = User::findOne(['id' => $_SESSION['user_id']]);
            $check = User::findOne(['username' => $_GET['phone']]);
            if ($check) {
                return 'Телефон уже зарегистрирован!';
            } else {
                $user->username = $_GET['phone'];
                if ($user->save(false)) {
                    $profile = UserProfile::findOne(['user_id' => $_SESSION['user_id']]);
                    $this->sendInformationAboutRegistration($profile->fio, $user->email, $_SESSION['user_password'], $user->username);
                    session_destroy();
                    Yii::$app->user->login($user);

                    // ADD BALANCE
                    $balance = UserBalance::createUserBalanceForRegistration($user->id);
                    if ($balance) {
                        return 1;
                    } else {
                        return 'Ошибка добавление баланса!';
                    }
                    // END ADD BALANCE
                } else {
                    $user_error = "";
                    $user_errors = $user->getErrors();
                    foreach ($user_errors as $v) {
                        $user_error .= $v[0];
                        break;
                    }
                    return $user_error;
                }
            }
        }else{
            throw new NotFoundHttpException();
        }


    }

    public function actionIndex($tab){

        if(Yii::$app->user->isGuest) return $this->goHome();
        $user = User::findOne(Yii::$app->user->id);
        $profile = UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
        $address = UserAddress::findAll(['user_id'=>Yii::$app->user->id]);
        $orders = Orders::getAll(6);
        $balance = UserBalance::findOne(['user_id'=>Yii::$app->user->id]);
        $gift = UserGift::findAll(['user_id'=>Yii::$app->user->id]);
        $fav = UserFavorites::findAll(['user_id'=>Yii::$app->user->id]);
        if($fav != null){
            $arr = '(';
            foreach ($fav as $v){
                $arr.=$v->product_id.',';
            }
            $arr = substr($arr,0,strlen($arr)-1);
            $arr.= ')';
            $fav = Products::find()->where('id in '.$arr)->all();
        }

        if(isset($_GET['per-page'])){
            $tab = 'orders';
        }

        return $this->render('profile', compact('user','profile','address','orders','fav','balance','gift','tab'));
    }

    public function actionSignIn(){
        if(isset($_SESSION['user_id'])){
            return $this->render('confirm-telephone');
        }else{
            if(Yii::$app->user->isGuest){
                $this->view->registerMetaTag(['name' => 'robots', 'content' => "noindex, nofollow"]);
                return $this->render('login');
            }else{
                throw new NotFoundHttpException();
            }
        }
    }


    public function actionSignUp(){
        if(isset($_SESSION['user_id'])){
            return $this->render('confirm-telephone');
        }else{
            if(Yii::$app->user->isGuest){
                $model = new SignupForm();
                return $this->render('register',compact('model'));
            }else{
                throw new NotFoundHttpException();
            }

        }
    }

    public function actionLogin(){

        if (Yii::$app->request->isAjax) {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post())) {
                if ($user = $model->login()) {
                    return 1;
                } else {
                    $error = "";
                    $errors = $model->getErrors();

                    foreach ($errors as $v) {
                        $error .= $v[0];
                        break;
                    }
                    return $error;
                }
            }
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionRegister()
    {

        if (Yii::$app->request->isAjax) {
            $model = new SignupForm();
            $profile = new UserProfile();
            $address = new UserAddress();

            Yii::$app->session['fio'] = Yii::$app->request->post('UserProfile')['fio'];
            Yii::$app->session['email'] = Yii::$app->request->post('SignupForm')['email'];
            Yii::$app->session['username'] = Yii::$app->request->post('SignupForm')['username'];
            Yii::$app->session['address'] = Yii::$app->request->post('UserAddress')['address'][0];
            Yii::$app->session['password'] = Yii::$app->request->post('SignupForm')['password'];
            Yii::$app->session['password_verify'] = Yii::$app->request->post('SignupForm')['password_verify'];

            if ($model->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {
                if ($address['address'][0] != null) {
                    $password = $model->password;
                    if ($user = $model->signUp()) {
                        $profile->user_id = $user->id;
                        if ($profile->save()) {
                            foreach ($address['address'] as $v) {
                                $newAddress = new UserAddress();
                                $newAddress->user_id = $user->id;
                                $newAddress->address = $v;
                                $newAddress->save();
                            }
                            $this->sendInformationAboutRegistration($profile->fio, $user->email, $password, $user->username);
                            Yii::$app->getUser()->login($user);
                            $balance = UserBalance::createUserBalanceForRegistration($user->id);
                            if ($balance) {
                                return 1;
                            } else {
                                return 'Ошибка добавление баланса!';
                            }

                        }
                    } else {
                        $user_error = "";
                        $user_errors = $model->getErrors();
                        foreach ($user_errors as $v) {
                            $user_error .= $v[0];
                            break;
                        }
                        return $user_error;
                    }
                } else {
                    return 'Необходимо заполнить «Адрес».';
                }
            } else {
                $profile_error = "";
                $profile_errors = $profile->getErrors();
                foreach ($profile_errors as $v) {
                    $profile_error .= $v[0];
                    break;
                }

                return $profile_error;

            }
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionUpdateAccountData()
    {
        if (Yii::$app->request->isAjax) {

            if (Yii::$app->user->isGuest) return $this->goHome();
            $user = User::findOne(Yii::$app->user->id);
            $user->scenario = User::update_all_data;
            $profile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);
            $address = new UserAddress();

            if ($profile->date_of_birth == null) {
                $birthUpdateStatus = true;
            } else {
                $birthUpdateStatus = false;
                $old_birth = $profile->date_of_birth;
            }

            if ($user->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {

                if ($user->password != null) {
                    $user->scenario = User::update_all_data;
                } else {
                    $user->scenario = User::update_data;
                }

                if (!$birthUpdateStatus) {
                    $profile->date_of_birth = $old_birth;
                }

                if ($address['address'][0] != null) {
                    if ($user->validate()) {
                        $password = $user->password;
                        if ($password != null) {
                            $user->setPassword($password);
                        }

                        if ($profile->save() && $user->save()) {
                            if ($password != null) {
                                $this->sendInformationAboutNewPassword($user->email, $user->username, $password);
                            }
                            UserAddress::deleteAll(['user_id' => Yii::$app->user->id]);
                            foreach ($address['address'] as $v) {
                                $newAddress = new UserAddress();
                                $newAddress->user_id = Yii::$app->user->id;
                                $newAddress->address = $v;
                                $newAddress->save();
                            }
                            return 1;
                        }
                    } else {
                        $user_error = "";
                        $user_errors = $user->getErrors();
                        foreach ($user_errors as $v) {
                            $user_error .= $v[0];
                            break;
                        }
                        return $user_error;
                    }
                } else {
                    return 'Необходимо заполнить «Адрес».';
                }

            } else {
                $profile_error = "";
                $profile_errors = $profile->getErrors();
                foreach ($profile_errors as $v) {
                    $profile_error .= $v[0];
                    break;
                }
                return $profile_error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }



    public function actionAddToFavorite(){

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->user->isGuest) {
                $text = "Чтобы добавить товар в избранное нужно войти!";
                $status = 0;
            } else {
                $product_id = $_GET['product_id'];
                $favorite = UserFavorites::findByUserAndProduct(\Yii::$app->user->id, $product_id);
                $text = "";
                $status = 1;
                if ($favorite == null) {
                    $newFavorite = new UserFavorites();
                    if ($newFavorite->saveFavorite(\Yii::$app->user->id, $product_id)) $text = 'Товар добавлен в избранное!';
                } else {
                    $favorite->delete();
                    $text = 'Товар удален с избранного!';
                }
            }
            $array = ['text' => $text, 'status' => $status];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionDeleteFromFavorite()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->user->isGuest) return $this->goHome();

            $favorite = UserFavorites::find()->where('user_id=' . Yii::$app->user->id . ' AND product_id=' . $_GET['id'])->one();
            $fav = null;
            if($favorite) {
                $favorite->delete();
            }

            $favorites = UserFavorites::findAll(['user_id' => Yii::$app->user->id]);
            if ($favorites != null) {
                $arr = '(';
                foreach ($favorites as $v) {
                    $arr .= $v->product_id . ',';
                }
                $arr = substr($arr, 0, strlen($arr) - 1);
                $arr .= ')';
                $fav = Products::find()->where('id in ' . $arr)->all();
            }
            return $this->renderAjax('favorite', compact('fav'));

        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionForgetPassword(){
        if (Yii::$app->request->isAjax) {
            $reset = new ResetPasswordForm();
            $reset->email = $_GET['email'];
            if ($reset->validate()) {
                $check = User::findOne(['email' => $_GET['email']]);
                if ($check) {
                    $check->generatePasswordResetToken();
                    if ($check->save(false)) {
                        $profile = UserProfile::findOne(['user_id' => $check->id]);
                        $link = Yii::$app->request->hostInfo . '/account/confirm?token=' . $check->password_reset_token;
                        return $this->sendUpdatePasswordInstruction($check->email, $link, $profile->fio);
                    }
                } else {
                    return 'E-mail не зарегистрирован.';
                }
            } else {
                $error = "";
                $errors = $reset->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;

            }
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionConfirm($token)
    {
        if (empty($token) || !is_string($token) || null === $user = User::findByResetToken($token)) {
            echo 'Ошибка.';
        } else {
            $user->removePasswordResetToken();
            $newPassword = $user->getRandomPassword();
            $user->setPassword($newPassword);
            if($user->save(false)){
                Yii::$app->session['modal-pass-reset'] = 1;
                $this->sendNewPassword($user->email,$newPassword);
                return $this->redirect('/');
            }else{
                echo 'Oops, something is error!';
            }

//            \Yii::$app->session->addFlash('success', \Yii::t('main-message', 'Email успешно подтвержден. Вы можете войти на сайт.'));
        }
    }


   public function actionOrderDetails($id){

	   if(Yii::$app->user->isGuest) {
	       return $this->goHome();
       }

	   $this->setMeta('Детали заказа');
       $order = Orders::findOne($id);
       $products = $order->products;
       $gifts = $order->gift;

       return $this->render('order', compact('products','gifts'));
   }



    private function sendNewPassword($email, $password)
    {
        $data = Message::getGeneratedNewPasswordMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'от Добрая Аптека'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['password' => $password]));
        return $emailSend->send();
    }



    private function sendUpdatePasswordInstruction($email, $link, $fio)
    {
        $data = Message::getForgetPasswordMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'от Добрая Аптека'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['link' => $link, 'fio' => $fio]));
        return $emailSend->send();
    }



    private function sendInformationAboutNewPassword($email, $telephone, $password)
    {
        $data = Message::getNewPasswordMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'от Добрая Аптека'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['phone' => $telephone, 'password' => $password]));
        return $emailSend->send();
    }



    private function sendInformationAboutRegistration($fio, $email, $password, $telephone)
    {
        $data = Message::getRegisterMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'от Добрая Аптека'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['fio' => $fio, 'phone' => $telephone, 'password' => $password]));
        return $emailSend->send();
    }




}
