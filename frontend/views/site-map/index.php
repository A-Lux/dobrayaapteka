<div class="site-map">
    <div class="container">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-catalog-tab" data-toggle="pill" href="#v-pills-catalog" role="tab"
                aria-controls="v-pills-catalog" aria-selected="true">Каталог</a>
            <a class="nav-link" id="v-pills-address-tab" data-toggle="pill" href="#v-pills-address" role="tab"
                aria-controls="v-pills-address" aria-selected="false">Адреса аптек</a>
            <a class="nav-link" id="v-pills-delivery-tab" data-toggle="pill" href="#v-pills-delivery" role="tab"
                aria-controls="v-pills-delivery" aria-selected="false">Оплата и доставка</a>
            <a class="nav-link" id="v-pills-feedback-tab" data-toggle="pill" href="#v-pills-feedback" role="tab"
                aria-controls="v-pills-feedback" aria-selected="false">Обратная связь</a>
            <a class="nav-link" id="v-pills-blog-tab" data-toggle="pill" href="#v-pills-blog" role="tab"
                aria-controls="v-pills-blog" aria-selected="false">Блог</a>
        </div>
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-catalog" role="tabpanel" aria-labelledby="v-pills-catalog-tab">
                <div class="site-inner">
                    <div class="site-content">
                        <a href="">Лекарственные средства</a>
                        <ul>
                            <li>
                                <a href="">Лекарственные средства1</a>
                            </li>
                            <li>
                                <a href="">Лекарственные средства2</a>
                            </li>
                        </ul>
                    </div>
                    <div class="site-content">
                        <a href="">Витамины и БАДы</a>
                        <ul>
                            <li>
                                <a href="">Витамины и БАДы1</a>
                            </li>
                            <li>
                                <a href="">Витамины и БАДы2</a>
                            </li>
                        </ul>
                    </div>
                    <div class="site-content">
                        <a href="">Мать и Дитя</a>
                        <ul>
                            <li>
                                <a href="">Мать и Дитя1</a>
                            </li>
                            <li>
                                <a href="">Мать и Дитя2</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-address" role="tabpanel" aria-labelledby="v-pills-address-tab">...
            </div>
            <div class="tab-pane fade" id="v-pills-delivery" role="tabpanel" aria-labelledby="v-pills-delivery-tab">...
            </div>
            <div class="tab-pane fade" id="v-pills-feedback" role="tabpanel" aria-labelledby="v-pills-feedback-tab">...
            </div>
            <div class="tab-pane fade" id="v-pills-blog" role="tabpanel" aria-labelledby="v-pills-blog-tab">...
            </div>
        </div>
    </div>
</div>