<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow slideInLeft">
                        <div class="main-title gradient-text">
                            <h5><?= $Product->name ?></h5>
                        </div>
                    </div>
                    <div class="col-sm-4 wow fadeInLeft">
                        <div class="product-img">
                            <? if ($Product->discount) : ?>
                            <div class="sale" style="top: 10%;left: 0%;">
                                <img src="/images/sale.svg" />
                                <div class="sale">
                                    <p>-<?= $Product->discount ?>%</p>
                                </div>
                            </div>
                            <? endif; ?>
                            <img src="<?=$Product->getImage();?>" class="img-fluid" alt="">
<!--                            --><?// if ($Product->status_products) : ?>
<!--                            <div class="red-label">-->
<!--                                дефицит-->
<!--                            </div>-->
<!--                            --><?// endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-8 wow fadeInRight">
                        <div class="product-description">
                            <ul>
                                <li>
                                    <? if($Product->checkRemainder()):?>
                                    <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                    <? else:?>
                                    <p>Наличие</p><span style="color: #df2a2a;">Нет в наличии</span>
                                    <? endif;?>
                                </li>
                                <li>
                                    <p>Цена</p> <span class="gradient-text" style="padding-right: 5px;">
                                        <? if ($Product->discount):?>
                                        <span
                                            class="oldPrice"><?=number_format($Product->oldPrice, 0, '', ' ');?></span>
                                        <? endif;?>
                                        <span
                                            class="gradient-text"><?= number_format($Product->calculatePrice, 0, '', ' '); ?>
                                        </span> тг
                                </li>
                                <li>
                                    <p>Производитель</p><span>
                                        <? if ($Product->this_country != null) : ?>
                                        <?= $Product->this_country->name ?>
                                        <? endif; ?>
                                    </span>
                                </li>
                                <li>
                                    <? if($Product->checkRemainder()):?>
                                    <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                    <? else:?>
                                    <p>Наличие</p><span style="color: #df2a2a;">Нет в наличии</span>
                                    <? endif;?>
                                </li>
                                <li>
                                    <p>Описание</p><span class="product_description"><?= $Product->description ?></span>
                                </li>
                                <li class="product_btns">
                                    <p>Количество</p>
                                </li>
                                <li style="align-items:center">
                                    <p class="product_input">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                            class="minus" data-id="<?= $Product->id ?>">-</span>
                                        <input class="quantity input-text" min="1" value="<?= $Product->count ?>"
                                            data-id="<?= $Product->id ?>" id="countProduct<?= $Product->id ?>" type="number">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                            class="plus" data-id="<?= $Product->id ?>">+</span>
                                    </p>
                                    <div class="product_btn_wrap">
                                        <span
                                            class="gradient-text"><?= number_format($Product->calculatePrice, 0, '', ' '); ?>
                                        </span> тг
                                        <div class="basket">
                                            <button class="btn-in-basket" data-id="<?= $Product->id; ?>"
                                                data-slider-id="<?= isset($id) ? $id : ''; ?>"><img
                                                    src="/images/light-basket.png" alt=""></button>
                                        </div>
                                        <? if (Yii::$app->user->isGuest) : ?><a href="#"><i
                                                class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                        <? else : ?><i
                                            class="fas fa-heart like-active favorites <?= $Product->getFavoriteStatus() ? 'favorites-active' : ''; ?>"
                                            data-id="<?= $Product->id; ?>" title="Добавить в избранное"></i>
                                        <? endif; ?>
                                    </div>
                                </li>
                            </ul>
                        </div>


                    </div>
                    <div class="col-lg-9 col-sm-12 wow fadeInUp">
                        <div class="instruction">
                            <p data-toggle="collapse" class="collapsed" data-target="#demo"><img
                                    src="/images/collapse-arrow.png" alt="">Инструкция по применению</p>
                        </div>
                        <div id="demo" class="collapse">
                            <div class="collapse-text">
                                <?= $Product->text ?>
                            </div>
                        </div>


                        <div class="instruction">
                            <p data-toggle="collapse" class="collapsed" data-target="#ostatki"><img
                                    src="/images/collapse-arrow.png" alt="">Наличие в аптеках</p>
                        </div>
                        <div id="ostatki" class="collapse"> -->
                            <div class="collapse-text">
                                <div class="box" id='result'>
                                    <? if ($remainder != null) : ?>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>Аптека</th>
                                                    <th>Количество</th>
                                                </tr>
                                            </thead>
                                            <tbody id="nearlyPharmaciesData">
                                                <? foreach ($remainder as $v) : ?>
                                                <tr>
                                                    <td><?= $v->filialNumber; ?></td>
                                                    <td><?= $v->filialName; ?></td>
                                                    <td>
                                                        <? if($v->amount > 0):?>
                                                        <?= $v->amount; ?>
                                                        <? else:?>
                                                        Нет в наличии
                                                        <? endif;?>
                                                    </td>
                                                </tr>
                                                <? endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <? else : ?>
                                    <div class="box-body">
                                        Нет
                                    </div>
                                    <? endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="accordion">
                                <div class="accordion-btn">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        Инструкция
                                    </button>
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                        Наличие
                                    </button>
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Отзывы
                                    </button>
                                </div>
                                <div class="card">
                                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne"
                                        data-parent="#accordion">
                                        <div class="card-body">
                                            <?= $Product->text ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                        data-parent="#accordion">
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>№</th>
                                                        <th>Аптека</th>
                                                        <th>Количество</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="nearlyPharmaciesData">
                                                    <? foreach ($remainder as $v) : ?>
                                                    <tr>
                                                        <td><?= $v->filialNumber; ?></td>
                                                        <td><?= $v->filialName; ?></td>
                                                        <td>
                                                            <? if($v->amount > 0):?>
                                                            <?= $v->amount; ?>
                                                            <? else:?>
                                                            Нет в наличии
                                                            <? endif;?>
                                                        </td>
                                                    </tr>
                                                    <? endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                        data-parent="#accordion">
                                        <div class="card-body">
                                            В разработке
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <? if ($sop_tovary != null) : ?>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="main-title gradient-text border-top">
                            <h5>Сопутствующие товары</h5>
                        </div>
                    </div>
                    <?= $this->render('../partials/sop_tovary', compact('sop_tovary')) ?>
                    <? endif; ?>
                    <? if ($analogy != null) : ?>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="product-title gradient-text border-top">
                            <h5>Аналоги лекарственных средств</h5>
                        </div>
                    </div>
                    <?= $this->render('../partials/analogy', compact('analogy')) ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp mb-5">
                        <div class="product-title gradient-text">
                            <h5><?= $Product->name ?></h5>
                        </div>
                        <div class="product-img">
                            <? if ($Product->discount) : ?>
                            <div class="sale" style="top: 15%;left: 0%;">
                                <img src="/images/sale.svg" />
                                <div class="sale">
                                    <p>-<?= $Product->discount ?>%</p>
                                </div>
                            </div>
                            <? endif; ?>
                            <img src="<?=$Product->getImage();?>" class="img-fluid" alt="">
                            <? //if ($Product->status_products != null):?>
                            <!-- <div class="red-label">
                                дефицит
                            </div> -->
                            <? //endif;?>
                        </div>
                        <div class="product-description">
                            <ul>
                                <li>
                                    <p>Цена</p> <span class="gradient-text" style="padding-right: 5px;">
                                        <?= number_format($Product->calculatePrice, 0, '', ' '); ?>
                                    </span>
                                    <span> тг</span>
                                </li>
                                <li>
                                    <p>Производитель</p><span>
                                        <? if ($Product->this_country != null) : ?>
                                        <?= $Product->this_country->name ?>
                                        <? endif; ?>
                                    </span>
                                </li>
                                <li>
                                    <? if($Product->checkRemainder()):?>
                                    <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                    <? else:?>
                                    <p>Наличие</p><span style="color: #df2a2a;">Нет в наличии</span>
                                    <? endif;?>
                                </li>
                                <li>
                                    <p class='product-description-content'>Описание</p><span class='product-description-content'><?= $Product->description ?></span>
                                </li>
                                <li>
                                    <p class='product-description-content'>Количество</p>
                                </li>
                            </ul>
                        </div>
                        <div class="mob-inform-product">
                            <div class="row">
                                <div class="col-4">
                                    <p class="product_input">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                            class="minus" data-id="<?= $v->id ?>">-</span>
                                        <input class="quantity input-text" min="1" value="" data-id=""
                                            id="countProduct<?= $v->id ?>" type="number">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                            class="plus" data-id="<?= $v->id ?>">+</span>
                                    </p>
                                </div>
                                <div class="col-8">
                                    <div class="button-wrap">
                                        <span
                                            class="gradient-text"><?= number_format($Product->calculatePrice, 0, '', ' '); ?>
                                        </span> тг
                                        <? if($Product->checkRemainder()):?>
                                        <div class="basket-button">
                                            <button class="btn-in-basket" data-id="<?= $Product->id; ?>"><img
                                                    src="/images/light-basket.png" alt=""></button>
                                        </div>
                                        <? endif;?>
                                        <a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-12 p-0">
                            <div id="accordionMob">
                                <div class="accordion-btn">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Инструкция
                                    </button>
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Наличие
                                    </button>
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Отзывы
                                    </button>
                                </div>
                                <div class="card">
                                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne"
                                        data-parent="#accordionMob">
                                        <div class="card-body">
                                            <?= $Product->text ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                        data-parent="#accordionMob">
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>№</th>
                                                        <th>Аптека</th>
                                                        <th>Количество</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="nearlyPharmaciesData">
                                                    <? foreach ($remainder as $v) : ?>
                                                    <tr>
                                                        <td><?= $v->filialNumber; ?></td>
                                                        <td><?= $v->filialName; ?></td>
                                                        <td>
                                                            <? if($v->amount > 0):?>
                                                            <?= $v->amount; ?>
                                                            <? else:?>
                                                            Нет в наличии
                                                            <? endif;?>
                                                        </td>
                                                    </tr>
                                                    <? endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                        data-parent="#accordionMob">
                                        <div class="card-body">
                                            В разработке
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <? if ($sop_tovary != null) : ?>
                    <div class="col-sm-12 wow fadeInUp">
                        <div
                            class="product-title gradient-text border-top d-flex align-items-center justify-conten-between">
                            <h5>Сопутствующие товары</h5>
                            <img class="owl-arrow-right" src="/backend/web/images/catalogproducts/arrow-right.png"
                                alt="">
                        </div>
                        <?= $this->render('../partials/sop_tovary', compact('sop_tovary')) ?>
                    </div>
                    <? endif; ?>

                    <? if ($analogy != null) : ?>
                    <div class="col-sm-12 wow fadeInUp">
                        <div
                            class="product-title gradient-text border-top d-flex align-items-center justify-conten-between">
                            <h5>Аналоги лекарственных средств</h5>
                            <img class="owl-arrow-right2" src="/backend/web/images/catalogproducts/arrow-right.png"
                                alt="">
                        </div>
                        <?= $this->render('../partials/analogy', compact('analogy')) ?>
                    </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>