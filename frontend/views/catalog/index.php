<div class="main">
    <? if (!isMobile()) : ?>
        <div class="desktop-version">
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInLeft">
                            <?= $this->render('/partials/catalog'); ?>
                            <div class="main-title gradient-text">
                                <h5 style="margin: 0;">Товар месяца</h5>
                            </div>
                            <? $m = 0 ;?>
                            <? foreach ($top_products as $v) : ?>
                                <? $m++;?>
                                <? if($m <= 16):?>
                                <div class="month-sale-item">
                                    <a href="">
                                        <div class="top">

                                            <div class="sale_discount">
                                                <div class="top_month">
                                                    <p>Товар месяца</p>
                                                </div>
                                            </div>

                                            <? if ($v->bonus && false) : ?>
                                                <div class="bonus">
                                                    <?= $v->bonus ?> Бонус
                                                </div>
                                            <? endif; ?>

                                            <div class="image">
                                                <a href="<?=$v->getUrl();?>">
                                                    <img src="<?=$v->getImage();?>" >
                                                </a>
                                            </div>
                                            <div class="text-block">
                                                <? //if ($v->status_products) { ?>
                                                    <!-- <div class="red-label">
                                                        дефицит
                                                    </div> -->
                                                <? //} ?>
                                                <div class="name">
                                                    <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                                                        <p><?= $v->name ?></p>
                                                    </a>
                                                </div>
                                                <div class="price">
                                                    <? if ($v->discount):?>
                                                        <p><?= number_format($v->oldPrice, 0, '', ' '); ?> тг</p>
                                                    <? endif;?>
                                                    <span class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?> тг</span>
                                                    
                                                </div>
                                                <div class="bot">
                                                    <span class="gradient-in">
                                                        <img src="/images/true_check.svg" alt="">
                                                        <span>В наличии</span>
                                                    </span>
                                                    <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                    <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                    <? endif; ?>

                                                    <div class="basket">
                                                        <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="<?= isset($id) ? $id : ''; ?>"><img src="/images/light-basket.png" alt=""></button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <? endif;?>
                            <? endforeach; ?>
                        </div>
                        <div class="col-lg-9 col-md-6 col-sm-12 wow fadeInRight">
                            <div class="white-bg wow fadeInRight">
                                <div class="title gradient-text wow fadeInRight">
                                    <h6><?= $catalog->name; ?></h6>
                                </div>
                            </div>
                            <div class="tablet-version-hide filter wow fadeInUp">
                                <div class="row">
                                    
                                    <div class="col-sm-6">
                                        <div class="filter-form">
                                            <label for="from">Цена от:</label>
                                            <input type="text" id="from" class="from_price">
                                            <label for="to">Цена до:</label>
                                            <input type="text" id="to" class="to_price">
                                            <label for="to">тг</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="checkbox-flex">
                                            <!-- <label class="filter-checkbox">В наличии
                                                <input type="checkbox" checked="checked" class="v_nalichii">
                                                <span class="checkmark"></span>
                                            </label> -->
                                            <label class="filter-checkbox">Акции
                                                <input type="checkbox" class="akcii">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tablet-version-hide items">
                                <div class="row" id="loadMoreResults">
                                    <? foreach($catalog->adsProduct as $adsProduct): ?>
                                        <?=$this->render('/partials/desktop_product', ['product' => $adsProduct->product, 'class' => 'col-sm-12 col-md-12 col-lg-4 wow fadeInUp']);?>
                                    <? endforeach; ?>

                                    <input type="hidden" value="<?= $catalog->id ?>" class="category_id">
                                    <? $m = 0; ?>
                                    <? foreach ($products as $v) : ?>
                                        <? $m++;?>
                                        <? if ($m > $per_page) break; ?>
                                        <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-4 wow fadeInUp']);?>
                                    <? endforeach; ?>

                                </div>
                                <div class="row" id="loadMoreButton">
                                    <? if (count($products) > $per_page) : ?>
                                        <div class="col-sm-12">
                                            <div class="collapse-wrap">
                                                <div class="load-more" id="load-more">
                                                    <button>Загрузить еще</button>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalog"></div>
                                    <!--                                    --><? //= $this->render("_bonus_web"); 
                                                                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <? else : ?>

        <div class="mobile-version">
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="title gradient-text wow fadeInRight">
                                <h6><?= $catalog->name; ?></h6>
                            </div>
                            <div class="owl1 owl-carousel owl-theme" style='opacity: 0; transtion: all .3s ease;'>
                                <? foreach ($categories as $v) : ?>
                                    <div class="item" ">
                                        <div class=" green">
                                        <a href="<?= $v->getUrl(); ?>"><?= $v->name; ?></a>
                                    </div>
                            </div>
                        <? endforeach; ?>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="filter">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="filter-form">
                                        <label for="fromMob">Цена от:</label>
                                        <input type="text" id="fromMob" class="from_price_mob">
                                        <label for="toMob">Цена до:</label>
                                        <input type="text" id="toMob" class="to_price_mob">
                                        <label for="toMob">тг</label>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox-flex">
                                        <!-- <label class="filter-checkbox">В наличии
                                            <input type="checkbox" checked="checked" class="v_nalichiiMob">
                                            <span class="checkmark"></span>
                                        </label> -->
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox" class="akciiMob">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-item tab-item-active" id="home">
                                <input type="hidden" value="<?= $catalog->id ?>" class="category_id">
                                <div class="row" id="loadMoreResultsMob">
                                    <? foreach($catalog->adsProduct as $adsProduct): ?>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="catalog-item">
                                                <div class="image">
                                                    <a href="<?=$adsProduct->product->getUrl();?>">
                                                        <img src="<?=$adsProduct->product->getImage();?>" >
                                                    </a>
                                                </div>
                                                <div class="text-block">
                                                    <? //if ($adsProduct->product->status_products) { ?>
                                                        <!-- <div class="red-label">
                                                            дефицит
                                                        </div> -->
                                                    <? //} ?>
                                                    <div class="name">
                                                        <a href="<?=$adsProduct->product->getUrl();?>" style="text-decoration: none;">
                                                            <p><?= $adsProduct->product->name ?></p>
                                                        </a>
                                                    </div>
                                                    <div class="status">

                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>

                                                        <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                        <? else : ?><i class="fas fa-heart like-active favorites <?= $adsProduct->product->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $adsProduct->product->id; ?>" title="Добавить в избранное"></i>
                                                        <? endif; ?>
                                                    </div>
                                                    <div class="flex-one">
                                                        <div class="price">
                                                            <? if ($v->discount):?>
                                                                <p><?= number_format($v->oldPrice, 0, '', ' '); ?></p>
                                                            <? endif;?>
                                                            <span class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?> </span> тг
                                                        </div>
                                                        <div class="catalog-basket-button">
                                                            <button class="btn-in-basket" data-id="<?= $adsProduct->product->id; ?>" data-slider-id="relatedProductsForCatalogMob">
                                                                <img src="/images/light-basket.png" alt="">В корзину</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                    <? $m = 0; ?>
                                    <? foreach ($products as $v) : ?>
                                        <? $m++;?>
                                        <? if ($m > $per_page) break; ?>
                                        <?=$this->render('/partials/mobile_product', ['product' => $v, 'class' => 'col-lg-3 col-md-6 col-sm-12 col-6  p-0']);?>
                                    <? endforeach; ?>
                                </div>

                                <div class="row" id="loadMoreButtonMob">
                                    <? if (count($products) > $per_page) : ?>
                                        <div class="col-sm-12">
                                            <div class="collapse-wrap">
                                                <div class="load-more" id="load-more-mob">
                                                    <button>Загрузить еще</button>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalogMob"></div>
                                    <!--                                    --><? //= $this->render("_bonus_mobile"); 
                                                                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <? endif; ?>
</div>
</div>
