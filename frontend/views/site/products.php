<? $m = 0; ?>
<? foreach ($model as $v) : ?>
    <? $m++; ?>
    <? if ($m < $count) : ?>

        <?=$this->render('/partials/desktop_product', ['product' => $v, 'id' => $id , 'class' => 'col-lg-3 col-md-6 col-sm-12 wow fadeInUp']);?>

    <? endif; ?>
<? endforeach; ?>
