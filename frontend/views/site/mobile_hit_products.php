<? $m = 0; ?>
<? foreach ($model as $v) : ?>
    <? $m++; ?>
    <? if ($m < $count) : ?>
        <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
            <a href="<?=$v->getUrl();?>">
                <div class="new-item">
                    <? if ($v->discount) : ?>
                        <div class="sale">
                            <img src="/images/sale.svg" style="height: auto;"/>
                            <div class="sale" style="left: -7px;top: 10px;">
                                <p>-<?= $v->discount ?>%</p>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="image">
                        <img src="<?=$v->getImage();?>" >
                    </div>
                    <div class="text-block">
                        <div class="name">
                            <p><?= $v->name ?></p>
                        </div>
                        <div class="status">

                            <p><img src="/images/ok.png" alt="">Есть в наличии</p>

                            <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                            <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>"></i>
                            <? endif; ?>
                        </div>
                        <div class="flex-one">
                            <div class="price">
                                <? if ($v->discount):?>
                                    <p><?= number_format($v->oldPrice, 0, '', ' '); ?></p>
                                <? endif;?>
                                <span class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?> </span> тг
                            </div>
                            <div class="news-basket-button">
                                <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="<?= $id; ?>">
                                    <img src="/images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <? endif; ?>
<? endforeach; ?>
