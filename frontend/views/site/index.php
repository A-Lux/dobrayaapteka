<div class="main">

    <? if (!isMobile()) : ?>

        <div class="desktop-version">
            <div class="slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-12 wow fadeInLeft">
                            <?= $this->render('/partials/catalog'); ?>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-12 wow fadeInRight">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <? $class = 'active'; ?>
                                    <? $m = 0; ?>
                                    <? foreach ($banner as $v) : ?>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="<?= $m; ?>" class="<?= $class ?>"></li>
                                        <? $class = ''; ?>
                                        <? $m++; ?>
                                    <? endforeach; ?>
                                </ol>
                                <div class="carousel-inner">
                                    <? $class = 'active'; ?>
                                    <? foreach ($banner as $v) : ?>

                                        <div class="carousel-item <?= $class; ?>">
                                            <? if ($v->url != null) : ?>
                                                <a href="<?= $v->url; ?>" onclick="gtag(['_trackEvent', 'internal promo', 'home­page­banner', this.href]);">
                                                    <img class="d-block" src="<?= $v->getImage(); ?>" alt="First slide">
                                                </a>
                                            <? else : ?>
                                                <img class="d-block" src="<?= $v->getImage(); ?>" alt="First slide">
                                            <? endif; ?>
                                            <div class="carousel-caption">
                                                <?= $v->text; ?>
                                            </div>
                                            <div class="green-caption">
                                                <img src="//images/green-caption.png" class="img-fluid" alt="">
                                            </div>
                                        </div>

                                        <? $class = ''; ?>
                                    <? endforeach; ?>
                                </div>

                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <img src="/images/left.png" alt="">
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <img src="/images/right.png" alt="">
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-content">
                <div class="container">
                    <div class="row">

                        
                        

                        <? if ($topMonthProducts != null) : ?>
                            <div class="col-sm-12">
                                <div class="title-border"></div>
                                <div class="main-title gradient-text wow fadeInUp">
                                    <h5>Товар месяца</h5>
                                </div>
                            </div>
                            <div id="topMonthProducts" class="row">
                                <?= $this->render('products', ['model' => $topMonthProducts, 'id' => 'relatedProductsForMonth', 'count' => 5]) ?>
                            </div>

                            <? if (count($topMonthProducts) > 4) : ?>
                                <div class="col-xl-12" id="topMonthProductLoadMore">
                                    <div class="top-month-product-load-more">
                                        <button data-id="relatedProductsForMonth">Загрузить еще</button>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="col-sm-12 wow fadeInUp" id="relatedProductsForMonth"></div>
                        <? endif; ?>

                        <? if(count($sliders) > 0):?>
                            <div class="col-sm-12">
                                <div class="row">
                                    <? foreach ($sliders as $v):?>
                                        <? if($v->url != null):?>
                                            <a href="<?=$v->url;?>" class="rek_item col-sm-4">
                                                <img src="<?=$v->getImage();?>" >
                                            </a>
                                        <? else:?>
                                            <a class="rek_item col-sm-4">
                                                <img src="<?=$v->getImage();?>" >
                                            </a>
                                        <? endif;?>
                                    <? endforeach;?>
                                </div>
                            </div>
                        <? endif;?>

                        <? if ($hitProducts != null) : ?>
                            <div class="col-sm-12">
                                <div class="title-border"></div>
                                <div class="main-title gradient-text wow fadeInUp">
                                    <h5><?= $titles[2]->name; ?></h5>
                                </div>
                            </div>
                            <div id="hitProducts" class="row">
                                <?= $this->render('products', ['model' => $hitProducts, 'isHit' => 1,  'id' => 'relatedProductsForHit', 'count' => 5]) ?>
                            </div>
                            <? if (count($hitProducts) > 4) : ?>
                                <div class="col-xl-12" id="hitProductLoadMore">
                                    <div class="hit-product-load-more">
                                        <button data-id="relatedProductsForHit">Загрузить еще</button>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="col-sm-12 wow fadeInUp" id="relatedProductsForHit"></div>
                        <? endif; ?>





                        <div class="col-sm-12">
                            <div class="title-border"></div>
                            <div class="main-title gradient-text">
                                <h5>Как нас найти</h5>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="map-block">
                                <div class="map wow fadeInLeft">
                                    <div id="map" style="width: 100%; height: 480px;border:0"></div>

                                    <script>
                                        window.onload = function() {
                                            var placeMarks = {};
                                            var places = document.querySelectorAll('.drugstore-item');
                                            ymaps.ready(init);

                                            var center_map = [0, 0];
                                            var map = "";

                                            function init() {
                                                map = new ymaps.Map('map', {
                                                    center: center_map,
                                                    zoom: 10,
                                                    controls: ['zoomControl','fullscreenControl']
                                                });
                                                map.behaviors.disable('scrollZoom');

                                                var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
                                                myGeocoder.then(
                                                    function(res) {
                                                        var street = res.geoObjects.get(0);
                                                        var coords = street.geometry.getCoordinates();
                                                        map.setCenter(coords);
                                                    },
                                                    function(err) {

                                                    }
                                                );
                                                <? foreach ($filial as $v) { ?>
                                                    placeMarks[<?= $v->id ?>] = new ymaps.Placemark([<?= $v->latitude ?>, <?= $v->longitude ?>], {
                                                        balloonContent: '<div class="company-name wow fadeInUp"><?= $v->address ?></br><?= $v->telephone ?></div>'
                                                    }, {
                                                        iconLayout: 'default#image',
                                                        iconImageHref: '/images/loc-icon.png',
                                                        preset: 'islands#icon',
                                                        iconColor: '#0095b6'
                                                    });
                                                    map.geoObjects.add(placeMarks[<?= $v->id ?>]);
                                                <? } ?>
                                                for (let key in placeMarks) {
                                                    places[+key - 1].addEventListener('click', function() {
                                                        placeMarks[key].events.fire('click');
                                                    })
                                                }

                                            }
                                        }
                                    </script>
                                </div>
                                <div class="drugstore-list wow fadeInRight">
                                    <? foreach ($filial as $v) : ?>
                                        <div class="drugstore-item">
                                            <p><img src="//images/loc-icon.png" alt="">
                                                <b><?= $v->name; ?>, <?= $v->address; ?></b></p>
                                            <? try {
                                                $from_time = new DateTime($v->from_time);
                                                $to_time = new DateTime($v->to_time);
                                            } catch (Exception $e) {
                                            } ?>
                                            <p><img src="//images/clock.png" alt=""><?= $from_time->format('H:i'); ?> - <?= $to_time->format('H:i'); ?> ч.</p>
                                            <p>
                                                <a href="tel:<?= $v->telephone; ?>">
                                                    <img src="//images/phone-icon.png" alt=""><?= $v->telephone; ?>
                                                </a>
                                            </p>
                                            <? if ($v->currentStatus == 'Открыто') : ?>
                                                <div class="status-open">
                                                    <?= $v->currentStatus; ?>
                                                </div>
                                            <? endif ?>
                                            <? if ($v->currentStatus == 'Закрыто') : ?>
                                                <div class="status-close">
                                                    <?= $v->currentStatus; ?>
                                                </div>
                                            <? endif ?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="main-title gradient-text wow fadeInUp advantages-subtitle">
                    <p><?= $titles[1]->name; ?></p>
                </div>
            </div>
            <div class="container">
                <div class="row">
                <? foreach (Yii::$app->view->params['advantages'] as $v) : ?>
                    <div class="col-lg-3 col-md-6 col-sm-3 wow fadeInUp" data-wow-delay=".2s">
                        <div class="adv-item">
                            <a href="advantage/<?= $v->url; ?>">
                                <div class="adv-icon">
                                    <img src="<?= $v->getImage(); ?>" alt="">
                                </div>
                                <div class="text-block">
                                    <h6><?= $v->name; ?></h6>
                                    <p><?= $v->text; ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                <? endforeach; ?>
                </div>
            </div>
        </div>
        
    <? else : ?>

        <div class="mobile-version">
            <div class="slider">
                <div class="container">
                    <div class="row wow fadeInUp">
<!--                        <div class="col-sm-12">-->
<!--                            <div class="owl1 owl-carousel owl-theme" style='opacity: 0; transtion: all .3s ease;'>-->
<!--                                --><?// foreach ($catalog as $value) : ?>
<!--                                    <div class="item">-->
<!--                                        <div class="green">-->
<!--                                            <a href="/catalog/--><?//= $value->url ?><!--">--><?//= $value->name; ?><!--</a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?// endforeach; ?>
<!--                            </div>-->
<!---->
<!--                        </div>-->
                        <div class="col-sm-12 pl-0 pr-0">
                            <div id="carouselExampleIndicators2" class="desktop-version carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <? $class = 'active'; ?>
                                    <? $m = 0; ?>
                                    <? foreach ($banner as $v) : ?>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="<?= $m; ?>" class="<?= $class ?>"></li>
                                        <? $class = ''; ?>
                                        <? $m++; ?>
                                    <? endforeach; ?>
                                </ol>

                                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                    <img src="/images/left.png" alt="">
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                    <img src="/images/right.png" alt="">
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="owl-main owl-carousel owl-theme mobile-version owl-main-mobile">
                                <? $class = 'active'; ?>
                                <? foreach ($banner as $v) : ?>
                                    <div class="item">
                                        <? if ($v->url != null) : ?>
                                            <a href="<?= $v->url; ?>" onclick="gtag(['_trackEvent', 'internal promo', 'home­page­banner', this.href]);">
                                                <img class="d-block w-100 img-fluid" src="<?= $v->getImage(); ?>" alt="First slide">
                                            </a>
                                        <? else : ?>
                                            <img class="d-block w-100 img-fluid" src="<?= $v->getImage(); ?>" alt="First slide">
                                        <? endif; ?>
                                        <div class="carousel-caption">
                                            <?= $v->text; ?>
                                        </div>
                                        <div class="green-caption">
                                            <img src="/images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <? $class = ''; ?>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="main-content">
                <div class="container">
                    <div class="row wow fadeInUp">
                        <? if ($topMonthProducts != null) : ?>
                            <div class="col-sm-12 wow fadeInUp">
                                <div class="main-title gradient-text">
                                    <h5>Товар месяца</h5>
                                </div>
                            </div>
                            <div class="col-sm-12 wow fadeInUp ">
                                <div class="square-flex" >
                                    <div id="mobileTopMonthProducts">
                                        <?= $this->render('mobile_products', ['model' => $topMonthProducts, 'id' => 'relatedProductsForMonthMobile', 'count' => 5]); ?>
                                    </div>
                                </div>
                            </div>
                            <? if (count($topMonthProducts) > 4) : ?>
                                <div class="col-xl-12" id="topMonthProductLoadMoreMobile" class="top-products-all">
                                    <div class="top-month-product-load-more-mob">
                                        <button data-id="relatedProductsForMonthMobile">Загрузить еще</button>
                                    </div>
                                </div>
                            <? endif; ?>

                            <div class="col-sm-12 wow fadeInUp" id="relatedProductsForMonthMobile"></div>

                        <? endif; ?>


                        <? if(count($sliders) > 0):?>
                            <div class="col-sm-12 pl-0 pr-0">
                                <div class="owl-main-mob owl-carousel owl-theme mobile-version owl-main-mobile">
                                    <? $class = 'active'; ?>
                                    <? foreach ($sliders as $v) : ?>
                                        <div class="item">
                                            <? if ($v->url != null) : ?>
                                                <a href="<?= $v->url; ?>" onclick="gtag(['_trackEvent', 'internal promo', 'home­page­banner', this.href]);">
                                                    <img class="d-block w-100 img-fluid" src="<?= $v->getImage(); ?>" alt="First slide">
                                                </a>
                                            <? else : ?>
                                                <img class="d-block w-100 img-fluid" src="<?= $v->getImage(); ?>" alt="First slide">
                                            <? endif; ?>
                                            <div class="green-caption">
                                                <img src="/images/green-caption.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <? $class = ''; ?>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? endif;?>


                        <? if ($hitProducts != null) : ?>

                            <div class="col-sm-12 wow fadeInUp ">
                                <div class="title-border"></div>
                                <div class="main-title gradient-text">
                                    <h5><?= $titles[2]->name; ?></h5>
                                </div>
                            </div>


                            <div class="col-sm-12 wow fadeInUp ">
                                <div class="square-flex" >
                                    <div id="mobileHitProducts">
                                        <?= $this->render('mobile_products', ['model' => $hitProducts, 'id' => 'relatedProductsForHitMobile', 'count' => 5]); ?>
                                    </div>
                                </div>
                            </div>
                            

                            <? if (count($hitProducts) > 4) : ?>
                                <div class="col-xl-12" id="hitProductLoadMoreMob" class="top-products-all">
                                    <div class="hit-product-load-more-mob">
                                        <button data-id="relatedProductsForHitMobile">Загрузить еще</button>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="col-sm-12 wow fadeInUp" id="relatedProductsForHitMobile"></div>

                        <? endif; ?>

                        <div class="col-sm-12 wow fadeInUp">
                            <div class="title-border"></div>
                            <div class="main-title gradient-text">
                                <h5>Как нас найти</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map wow fadeInUp">
                <div id="mapMob" style="width: 100%; height: 480px;border:0"></div>
                <script>
                    ymaps.ready(init);
                    var center_map = [0, 0];
                    var mapMob = "";

                    function init() {
                        mapMob = new ymaps.Map('mapMob', {
                            center: center_map,
                            zoom: 10,
                            controls: ['zoomControl','fullscreenControl']
                        });
                        // mapMob.behaviors.disable('drag');
                        var myGeocoder = ymaps.geocode("<?= Yii::$app->session['city_name']; ?>");
                        myGeocoder.then(
                            function(res) {
                                var street = res.geoObjects.get(0);
                                var coords = street.geometry.getCoordinates();
                                mapMob.setCenter(coords);
                            },
                            function(err) {

                            }
                        );

                        <? foreach ($filial as $v) { ?>
                            mapMob.geoObjects.add(new ymaps.Placemark([<?= $v->latitude ?>, <?= $v->longitude ?>], {
                                balloonContent: '<div class="company-name wow fadeInUp"><?= $v->address ?></br><?= $v->telephone ?></div>'
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/images/loc-icon.png',
                                preset: 'islands#icon',
                                iconColor: '#0095b6'
                            }));
                        <? } ?>
                    }
                </script>
            </div>
            <div class="col-sm-12">
                <div class="title-border"></div>
                <div class="main-title gradient-text">
                    <h5><?= $titles[1]->name; ?></h5>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="square-flex">
                    <? foreach (Yii::$app->view->params['advantages'] as $v) : ?>
                        <div class="adv-item">
                            <a href="advantage/<?= $v->url; ?>">
                                <div class="adv-icon">
                                    <img src="<?= $v->getImage(); ?>" alt="">
                                </div>
                                <div class="text-block">
                                    <h6><?= $v->name; ?></h6>
                                    <!-- <p><?= $v->text; ?></p> -->
                                </div>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    <? endif; ?>

</div>
</div>
