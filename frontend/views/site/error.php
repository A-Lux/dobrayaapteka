<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Не найдено (# 404)';
?>
<div class="site-error" style="height:350px;margin: 50px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        Страница не найдено
    </div>

    <p>
        Вышеуказанная ошибка произошла, когда веб-сервер обрабатывал ваш запрос.
    </p>
    <p>
        Пожалуйста, свяжитесь с нами, если считаете, что это ошибка сервера. Спасибо.
    </p>

</div>
