<? $m = 0; ?>
<? foreach ($model as $v) : ?>
    <? $m++; ?>
    <? if ($m < $count) : ?>

        <div class="top-sale-item">
            <? if ($v->isHit == 1) : ?>
                <div class="hit">
                    <img src="/images/hit.png" alt="">
                </div>
            <? endif; ?>
            <? if ($v->discount) : ?>
                <div class="sale">
                    <img src="/images/sale.svg" style="height: auto;" />
                    <div class="sale" >
                        <p>-<?= $v->discount ?>%</p>
                    </div>
                </div>
            <? endif; ?>
            <a href="<?=$v->getUrl();?>">
                <img src="<?=$v->getImage();?>" >
            </a>
            <div class="name">
                <p><?= $v->name ?></p>
            </div>
            <div class="price">
                <? if ($v->discount):?>
                    <p><?= number_format($v->oldPrice, 0, '', ' '); ?></p>
                <? endif;?>
                <span class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?> </span> тг
            </div>
            <div class="basket-button">
                <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="<?= $id; ?>">
                    <img src="/images/light-basket.png" alt="">В корзину</button>
            </div>
        </div>

    <? endif; ?>
<? endforeach; ?>
