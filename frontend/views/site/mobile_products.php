<div class="row">
<? $m = 0; ?>
<? foreach ($model as $v) : ?>
    <? $m++; ?>
    <? if ($m < $count) : ?>

        <?=$this->render('/partials/mobile_product', ['product' => $v, 'id' => $id, 'class' => 'col-lg-3 col-md-6 col-sm-12 col-6 mob_p wow fadeInUp']);?>

    <? endif; ?>
<? endforeach; ?>
</div>
