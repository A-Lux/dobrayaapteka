<div class="<?=$class;?>">

    <a href="<?=$product->getUrl();?>">
        <div class="top-sale-item">
            <div class="sale_img">
                <img src="<?=$product->getImage();?>" alt="">
            </div>
            <? if ($product->discount) : ?>
                <div class="sale_discount">
                    <div class="sale">
                        <p>-<?= $product->discount ?>%</p>
                    </div>
                </div>
            <? elseif($product->isHit):?>
                <div class="sale_discount">
                    <div class="hit">
                        <p>Топ продаж</p>
                    </div>
                </div>
            <? elseif($product->isNew):?>
                <div class="sale_discount">
                    <div class="new">
                        <p>Новинка</p>
                    </div>
                </div>
            <? elseif($product->top_month):?>
                <div class="sale_discount">
                    <div class="top_month">
                        <p>Товар месяца</p>
                    </div>
                </div>
            <? endif;?>

            <? // if ($product->status_products != null):?>
                <!-- <div class="sale_discount">
                    <div class="sale_status">
                        <p>дефицит</p>
                    </div>
                </div> -->
            <? // endif;?>
            <div class="sale_info">
<!--                <div class="sale_stars">-->
<!--                    <img src="/images/star_full.svg" alt="" />-->
<!--                    <img src="/images/star_full.svg" alt="" />-->
<!--                    <img src="/images/star_full.svg" alt="" />-->
<!--                    <img src="/images/star_empty.svg" alt="" />-->
<!--                    <img src="/images/star_empty.svg" alt="" />-->
<!--                </div>-->
                <div class="name">
                    <p><?= $product->name ?></p>
                </div>
                <div class="sale_bottom_block">
                    <div class="price">

                        <? if($class == ''):?>
                            <? if ($product->discount):?>
                                <p>
                                    <?= number_format($product->oldPrice, 0, '', ' '); ?> тг
                                </p>
                            <? endif;?>
                        <? else:?>
                            <p>
                                <? if ($product->discount):?>
                                    <?= number_format($product->oldPrice, 0, '', ' '); ?> тг
                                <? endif;?>
                            </p>
                        <? endif;?>

                        <span class="gradient-text">
                            <?= number_format($product->calculatePrice, 0, '', ' '); ?> тг
                        </span>
                        <span class="gradient-in">
                            <img src="/images/true_check.svg" alt="">
                            <span>В наличии</span>
                        </span>
                    </div>
                    <div class="favorite">
                        <? if (Yii::$app->user->isGuest) : ?>
                            <a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                        <? else : ?>
                            <a>
                                <i class="fas fa-heart like-active favorites <?= $product->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $product->id; ?>" title="Добавить в избранное"></i>
                            </a>
                        <? endif; ?>
                    </div>
                    <div class="basket">
                        <button class="btn-in-basket" data-id="<?= $product->id; ?>" data-slider-id="<?= isset($id) ? $id : ''; ?>"><img src="/images/light-basket.png" alt=""></button>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>