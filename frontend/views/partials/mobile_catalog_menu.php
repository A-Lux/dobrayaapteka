<? foreach ($catalog as $value): ?>
    <li>
        <? if($value->childs):?>
            <a>
                <? if($value->getMobileImage() != null):?>
                    <span class="menu-icon">
                        <img src="<?= $value->getMobileImage(); ?>" alt="">
                    </span>
                <? endif;?>
                <p><?= $value->name; ?></p>
            </a>

            <ul>
                <li>
                    <a href="<?=$value->getUrl();?>">
                        <p>Перейти в категорию</p>
                    </a>
                </li>
                <? if($value->childs):?>
                    <?=$this->render('/partials/mobile_catalog_menu', ['catalog' => $value->childs]);?>
                <? endif;?>
            </ul>
        <? else:?>
            <a href="<?=$value->getUrl();?>">
                <? if($value->getMobileImage() != null):?>
                    <span class="menu-icon">
                        <img src="<?= $value->getMobileImage(); ?>" alt="">
                    </span>
                <? endif;?>
                <p><?= $value->name; ?></p>
            </a>
        <? endif;?>
    </li>
<? endforeach; ?>
