<div class="owl1 owl-carousel owl-theme ">
    <? foreach ($sop_tovary as $v) : ?>

        <? if(!isMobile()):?>
            <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => ''])?>
        <? else:?>
            <?=$this->render('/partials/mobile_product', ['product' => $v, 'class' => ''])?>
        <? endif;?>

    <? endforeach; ?>
</div>
