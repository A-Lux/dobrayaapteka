<div class="catalog-menu">
   <div class="title gradient-text">
        Каталог товаров
    </div>
    <style>
        summary {
            display: flex;
            height: 57px;
        }
        .catalog-menu .title{
            margin-right: 10px;
        }
        ::-webkit-details-marker {
            display: none;
        }
        .catalog-wrapper{
            height: 347px;
            overflow-y: scroll;
        }
        .catalog-link-inform{
            width: 152px;
            padding: 0px 20px !important;
        }
        
    </style>
    <ul class="catalog-wrapper">
        <? foreach (Yii::$app->view->params['catalog'] as $k => $v):?>
        <? $childs = $v->childs;?>
        <li style="height: auto; overflow:auto; padding-right: 0 !important;">
            <details>
                <summary>
                    <span class="menu-icon">
                        <img src="<?=$v->getDesktopImage();?>" alt="">
                    </span>
                    <p>
                        <?=$v->name;?>
                    </p>
                </summary>
                    <ul class="second-level-menu px-0" id="category<?=$v->id;?>">
                        <? foreach ( $childs as $k => $v):?>
                            <li style="padding-right: 0 !important;">
                                <div class="row">
                                    <div id="div<?=$v->id;?>" style="margin-top: 10px; margin-bottom: 10px;" onclick="getSubs(<?=$v->id;?>, 0)"  class="col-sm-12">
                                        <?=$v->title;?>
                                        
                                    </div>
                                </div>
                                <ul id="category<?=$v->id;?>">
                                            
                                </ul>                                                    
                            </li>
                        <? endforeach;?>
                    </ul>
                </details>
            <? /* <div class="modal-drug">

                <? $col = count($childs) / 2.0;?>
                <? $col2 = count($childs) % 2.0;?>
                <div class="modal-drug-menus">
                
                </div>

                <div class="link-catalog"><a href="<?= $v->getUrl(); ?>">Перейти к товарам</a></div>

                </div> */ ?>

        </li>
        <? endforeach;?>
    </ul>
</div>
