<div class="delivery-cost">
    <a href="#" data-toggle="modal" data-target="#exampleModalAddress">Укажите адрес забора на карте</a>
</div>

<div class="delivery-cost">
    <p>Адрес забора:</p>
    <div class="price">
        <p class="gradient-text" id="fenceAddress"></p>
    </div>
</div>



<!-- <div id="map" style="width: 100%; height: 300px; float:left;"></div> -->
<div id="viewContainer"></div>
<div class="modal modal-map fade" id="exampleModalAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalAddressLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="w-50 modal-h-content">
                    <h5 class="modal-title" id="exampleModalLabel">Адрес забора</h5>
                    <div class="search-map" id="searchResult">
                        <input type="text" id="addressSearch" class="pickupAddressInput" disabled>
                        <button class="btn-map" id="btnSavePickup" onclick="savePickupAddress()">Хорошо</button>
                        <button class="btn-map-disabled" id="btnSavePickupDisabled" disabled>Хорошо</button>
                    </div>
                </div>
                <div class="delivery-modal">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div id="mapAddress" style="width: 100%; height: 85vh; float:left;"></div>
                </div>
                <div class="col-sm-4" id="addressResult">
                    <? foreach ($address as $v) : ?>
                        <div style="cursor: pointer;" onclick="selectAddress(<?= $v->id; ?>)">
                            <p> <?= $v->address; ?> <br> <?= $v->telephone; ?></p>
                        </div>
                        <br>
                    <? endforeach; ?>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    ymaps.ready(init);
    var center_map = [0, 0];
    var mapAddress = "";

    function init() {
        mapAddress = new ymaps.Map('mapAddress', {
            center: center_map,
            zoom: 11,
            controls: ['zoomControl']
        });

        var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
        myGeocoder.then(
            function(res) {
                var street = res.geoObjects.get(0);
                var coords = street.geometry.getCoordinates();
                mapAddress.setCenter(coords);
            },

        );

        <? foreach ($address as $v) { ?>
            var myPlacemark = new ymaps.Placemark([<?= $v->latitude ?>, <?= $v->longitude ?>], {
                balloonContent: '<div class="company-name wow fadeInUp" onclick="selectAddress(<?= $v->id; ?>)"><?= $v->address ?></br><?= $v->telephone ?></div>'
            }, {
                iconLayout: 'default#image',
                iconImageHref: '/images/loc-icon.png',
                preset: 'islands#icon',
                iconColor: '#0095b6'
            });
            console.log(myPlacemark);
            mapAddress.geoObjects.add(myPlacemark);
            myPlacemark.events.add('click', function() {

                showLoader();
                $.ajax({
                    url: "/card/get-select-address",
                    type: "GET",
                    data: {
                        id: <?= $v->id; ?>
                    },
                    success: function(address) {
                        $('.pickupAddressInput').val(address);
                        $('#btnSavePickupDisabled').hide();
                        $('#btnSavePickup').show();
                        hideLoader();
                    },
                    error: function() {
                        hideLoader();
                        swal('Упс!', 'Что-то пошло не так.', 'error');
                    }
                });

            });
        <? } ?>
    }



    function savePickupAddress() {
        var address = $('.pickupAddressInput').val();
        $('#fenceAddress').html(address);
        $(".close").click();

        $.ajax({
            url: "/card/set-select-address",
            type: "GET",
            data: {
                address: address
            },
            success: function(address) {

            },

        });
    }



    function selectAddress(id) {
        showLoader();
        $.ajax({
            url: "/card/get-select-address",
            type: "GET",
            data: {
                id: id
            },
            success: function(address) {
                $('.pickupAddressInput').val(address);
                showPickupSaveButton();
                hideLoader();
            },
            error: function() {
                hideLoader();
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }


    function showPickupSaveButton() {
        $('#btnSavePickupDisabled').hide();
        $('#btnSavePickup').show();
    }



    function hidePickupSaveButton() {
        $('#btnSavePickupDisabled').show();
        $('#btnSavePickup').hide();

    }

    $('body').on('change paste keyup', '.pickupAddressInput', function() {
        hidePickupSaveButton();
        $('#fenceAddress').html("");
        $.ajax({
            url: "/card/set-select-address",
            type: "GET",
            data: {
                address: null
            },
            success: function(address) {

            },

        });
    })
</script>
