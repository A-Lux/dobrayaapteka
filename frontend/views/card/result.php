<div class="container">
    <div class="row">
        <div class="col-md-12 check">
            <div class="check_title">
                <h3>№<?=$order->id;?> заказ завершен!</h3>
                <p>Спасибо что вы выбрали наш интернет магазин Добрая аптека!
                    <? if(!Yii::$app->user->isGuest):?>
                    Отследить состояние заказа Вы можете в личном кабинете.
                    <? endif;?>
                </p>
<!--                <span>Ожидайте курьера 12 декабря 2019, в 10:00</span>-->
            </div>
            <? if(!Yii::$app->user->isGuest):?>
                <a href="<?=\yii\helpers\Url::toRoute(['/account/?tab=orders'])?>">Перейти в личный кабинет</a>
            <? endif;?>
            <div class="history_cart">
                <h3>Товары в заказе</h3>
                <ul>
                    <? if($order->products != null):?>
                        <? foreach ($order->products as $v):?>
                        <? if($v->product):?>
                            <li>
                                <img src="<?=$v->product->getImage();?>" alt="<?=$v->product->name;;?>">
                                <h3><?=$v->product->name;;?></h3>
                                <span><?=$v->count;?> шт.</span>
                                <p><?=number_format($v->product->calculatePrice * $v->count,0, '', ' ');?> <span>тг.</span></p>
                            </li>
                        <? endif;?>
                        <? endforeach;?>
                    <? endif;?>
                </ul>

                <hr />
                <div class="history_bottom_text">
                    <span>Использованная скидка по промокоду:</span>
                    <span class="history_bottom_number">
                        <?=number_format($order->used_promo,0, '', ' ')?> <span>%</span>
                    </span>
                </div>

                <hr />
                <div class="history_bottom_text">
                    <span>Использованная скидка для пенсионеров:</span>
                    <span class="history_bottom_number">
                        <?=number_format($order->used_elderly_discount,0, '', ' ')?> <span>%</span>
                    </span>
                </div>

                <hr />
                <div class="history_bottom_text">
                    <span>Использованный бонус:</span>
                    <span class="history_bottom_number">
                        <?=number_format($order->used_bonus,0, '', ' ')?> <span>тг.</span>
                    </span>
                </div>


                <hr />
                <div class="history_bottom_text">
                    <span>Доставка:</span>
                    <span class="history_bottom_number">
                        <?=number_format($order->deliverySum,0, '', ' ')?> <span>тг.</span>
                    </span>
                </div>

                <hr />
                <div class="history_bottom_text">
                    <span>Итого:</span>
                    <span class="history_bottom_number">
                        <?=number_format($order->sum,0, '', ' ')?> <span>тг.</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
