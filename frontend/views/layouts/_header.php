
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://api-maps.yandex.ru/2.1/?apikey=0b0a8929-e785-47a7-904a-87e8c66e8dcc&lang=ru_RU" type="text/javascript"></script>
<script>
    <? if (Yii::$app->session['modal-pass-reset']) : ?>
    swal("Уважаемый пользователь!", "Ваш старый пароль был сброшен. Новый пароль был отправлен на вашу электронную почту.");
    <? unset(Yii::$app->session['modal-pass-reset']); ?>
    <? endif; ?>
</script>
<style>
    .second-level-menu li{
            padding: 0 20px!important;
        }
    .catalog-link-inform{
        width: 190px !important;
    }
</style>
<? if(Yii::$app->view->params['notification'] != ''):?>
    <div class="message">
        <span><?=Yii::$app->view->params['notification'];?></span>
    </div>
<? endif;?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js" integrity="sha512-DZqqY3PiOvTP9HkjIWgjO6ouCbq+dxqWoJZ/Q+zPYNHmlnI2dQnbJ5bxAHpAMw+LXRm4D72EIRXzvcHQtE8/VQ==" crossorigin="anonymous"></script>
                        <script>
                            function close(id, level) {
                                document.getElementById('category'+id).innerHTML = "";
                                document.getElementById('div'+id).onclick = function(){ getSubs(id, level)};
                            }
                            function getSubs(id, level) {
                                var colors = [
                                    '#3bb575',
                                    '#37a96d',
                                    '#359c66',
                                    '#2f8a5a',
                                    '#276f49',
                                    '#20583a',
                                    '#19482f',
                                    '#143825'
                                ];
                                axios.get('/catalog/dynamic-catalog', {
                                    "params": {
                                        "category_id": id
                                    }
                                }).then(response => {
                                    response.data.forEach(element => {
                                        var template = `
                                            <style>
                                                .qwerty${id}:hover{
                                                    background-color: ${colors[level]} !important;
                                                    padding-right: 0 !important;
                                                }
                                            </style>
                                            <li class="qwerty${id} catalog-link-inform"  >
                                            <div class="row">
                                                <div style="margin-top: 10px; margin-bottom: 10px; width:150px !important" id="div${element.id}"  onclick="getSubs(${element.id}, ${level+1})">`;
                                                    if(element.childs == 0){
                                                        template += `<a href="/catalog/${element.url}">`
                                                    };
                                                    template += element.name;
                                                    if(element.childs == 0){
                                                        template += `</a>`
                                                    };
                                                    template += `</div>
                                            </div>
                                            <ul id="category${element.id}">
                                                        
                                            </ul>`;
                                        document.getElementById('category'+id).innerHTML += template;
                                    });
                                    console.log(id);
                                    document.getElementById('div'+id).onclick = function(){ close(id, level) };
                                });
                            }
                        </script>
<div class="overlay">
    <div class="pick-city-modal">
        <div class="close-modal">
            &#10005
        </div>
        <h2>Выберите ваш город</h2>
        <div class="city-list">

            <? $len = count(Yii::$app->view->params['cities']); ?>
            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                    <? if ($len / 3 >= $m) : ?>
                        <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                    <? endif; ?>
                    <? $m++; ?>
                <? endforeach; ?>

            </ul>

            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                    <? if ($len / 3 < $m && ($len / 3) * 2 >= $m) : ?>
                        <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                    <? endif; ?>
                    <? $m++; ?>
                <? endforeach; ?>
            </ul>
            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                    <? if (($len / 3) * 2 < $m) : ?>
                        <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                    <? endif; ?>
                    <? $m++; ?>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="content">

    <? if (!isMobile()) : ?>
       <div id="myHeader" class="desctop-header-sticky" style="z-index: 10000;">
       <div class="desktop-version">
            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                        <?php if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != ''):?>
                            <div class="logo wow fadeInLeft">
                                <a href="/">
                                    <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                                </a>
                            </div>
                        <?php else:?>
                            <div class="logo wow fadeInLeft">
                                <a href="/">
                                    <img class="img-desk" src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                                </a>
                            </div>
                        <?php endif;?>
                        </div>
                        <!-- <div class="col-sm-2 ">
                        <div class="loc" id="city">
                            <div class="loc-icon">
                                <img src="/images/loc-icon.png" alt="">
                            </div>
                            <span><?= Yii::$app->session['city_name']; ?> <img src="/images/arrow.png" alt=""></span>
                        </div>
                    </div> -->
                        <div class="col-sm-8 d-flex align-items-center col-md-7 col-xl-8">
                            <div class="menu wow fadeInDown">
                                <ul class="m-0">
                                    <? foreach (Yii::$app->view->params['menu'] as $v) : ?>
                                        <li><a href="/<?= $v->url; ?>"><?= $v->text; ?></a></li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-3 col-xl-2 p-0">
                            <a href="" class="call-center"><i class="fa fa-phone" aria-hidden="true"></i>+7 (747) 094-13-00</a>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div >
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-sm-3 col-md-3 col-lg-2">
                        <?php if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != ''):?>
                            <div class="catalog">
                                <a href="/catalog/<?= Yii::$app->view->params['catalog'][0]->url; ?>" class='toggle'>
                                    <span class="green_line"></span> 
                                </a>
                                <nav id="desktop-nav">
                                    <ul>
                                        <li><a href="#">Каталог</a>
                                            <ul>
                                                <?=$this->render('/partials/mobile_catalog_menu', ['catalog' => Yii::$app->view->params['catalogLevel1']]);?>
                                            </ul>
                                        </li>

                                        <? foreach (Yii::$app->view->params['menu'] as $v) : ?>

                                            <li><a href="/<?= $v->url; ?>"><i class="<?= $v->icon; ?>"></i><?= $v->text; ?></a></li>
                                            //
                                            <? if ($v->url == "/news") : ?>
                                                <li><a href="/<?= $v->url; ?>"><img src="/images/newspaper.png"><?= $v->text; ?></a></li>
                                            <? endif; ?>
                                            <? if ($v->url == "address") : ?>
                                                <ul>
                                                    <? foreach (Yii::$app->view->params['address'] as $v) : ?>
                                                        <li><a href="/site/shops"><?= $v->address; ?></a></li>
                                                    <? endforeach; ?>
                                                </ul>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <form class="search" method="get" action="/search">
                            <input type="text" id="searchbox" name="text" placeholder="Введите название товара">
                            
                            <button type="submit" class="search_btn"><img src="/images/search-ico.png" alt=""></button>
                        </form>
                    </div>
                    <div class="col-sm-1">
                        <div class="user-icon">
                            <? if (!Yii::$app->user->isGuest) : ?>
                                <a href="/account/?tab=home">
                                    <img src="/images/user-icon.png" alt="Личный кабинет" title="Личный кабинет">
                                </a>
                                <a href="/account/logout" style="margin-left: 20px;">
                                    <i class="fa fa-sign-out" aria-hidden="true" title="Выйти" ></i>
                                </a>
                            <? endif; ?>
                            <? if (Yii::$app->user->isGuest) : ?>
                                <a href="/account/sign-in">
                                    <img src="/images/user-icon.png" alt="Личный кабинет" title="Личный кабинет">
                                </a>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="basket-icon">
                            <a href="/card/">
                                <img src="/images/basket-icon.png" alt="Корзина" title="Корзина">
                                <span class="count count-add"><?= Yii::$app->view->params['count']; ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       </div>

    <? else : ?>

        <div class="mobile-version header-sticky">
            <header>
                <div class="col-2" style="display: flex">
                                            
                </div>
                <div class="container">
                    <div class="row wow fadeInDown p-1">
                        <div class="col-6">
                        
                    
                        <nav id="main-nav">
                            <ul>
                                <li><a href="#">Каталог</a>
                                    <ul>
                                        <?=$this->render('/partials/mobile_catalog_menu', ['catalog' => Yii::$app->view->params['catalogLevel1']]);?>
                                    </ul>
                                </li>

                                <? foreach (Yii::$app->view->params['menu'] as $v) : ?>

                                    <li><a href="/<?= $v->url; ?>"><i class="<?= $v->icon; ?>"></i><?= $v->text; ?></a></li>
                                    //
                                    <? if ($v->url == "/news") : ?>
                                        <li><a href="/<?= $v->url; ?>"><img src="/images/newspaper.png"><?= $v->text; ?></a></li>
                                    <? endif; ?>
                                    <? if ($v->url == "address") : ?>
                                        <ul>
                                            <? foreach (Yii::$app->view->params['address'] as $v) : ?>
                                                <li><a href="/site/shops"><?= $v->address; ?></a></li>
                                            <? endforeach; ?>
                                        </ul>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </ul>
                        </nav>
                        <div class="logo">
                            <a href="/">
                                <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                            </a>
                        </div>
                        </div>
                        <div class="col-2">
                            <!-- <a href="" class="call-center">+7 (747) 094-13-00</a> -->
                        </div>
                        <div class="col-4" style="display: flex; justify-content: flex-end; align-items: center; height: 2rem;">
                            <div class="user-icon">
                                <? if (!Yii::$app->user->isGuest) : ?>
                                    <a href="/account/?tab=home">
                                        <img src="/images/user-icon.png" alt="">
                                    </a>
                                    <a href="/account/logout" style="margin-left: 20px;"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                <? endif; ?>
                                <? if (Yii::$app->user->isGuest) : ?>
                                    <a href="/account/sign-in">
                                        <img src="/images/user-icon.png" alt="">
                                    </a>
                                <? endif; ?>
                            </div>
                            <div class="mobile-flex">
                                <div class="basket-icon">
                                    <a href="/card/">
                                        <img src="/images/basket-icon.png" alt="">
                                        <span class="count count-add"><?= Yii::$app->view->params['count']; ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div class="col-sm-12 fixed-search">
            <div class="d-flex justify-content-end">
                <form class="search" method="get" action="/search/index">
                    <input type="text" id="searchboxMobile" name="text" placeholder="Введите название товара">
                    <button type="submit" class="search_btn_mob"><img src="/images/search-ico.png" alt=""></button>
                </form>
            </div>
        </div>

    <? endif; ?>
