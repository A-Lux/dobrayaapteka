<footer class="footer">
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="col-sm-12">
                <div class="mobile-phone">
                    <a href="tel:+7 747 094-13-00"><img src="/images/mobile-phone.svg" alt=""></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer_list">
                    <h3>Информация</h3>
                    <ul>
                        <? foreach (Yii::$app->view->params['footer_information'] as $v):?>
                            <li><a href="/<?= $v->url; ?>"><?=$v->text;?></a></li>
                        <? endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer_list">
                    <h3>Преимущества</h3>
                    <ul>
                        <? foreach (Yii::$app->view->params['advantages'] as $advantage):?>
                            <li><a href="/advantage/<?= $advantage->url; ?>"><?= $advantage->name; ?></a></li>
                        <? endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer_list">
                    <h3>Мы в соцсетях</h3>
                    <p>
                        <a href="<?= Yii::$app->view->params['contact']->facebook; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a class="insta" href="<?= Yii::$app->view->params['contact']->instagram; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer_list">
                    <h3>Юридическая информация</h3>
                    <ul>
                        <? foreach (Yii::$app->view->params['legal_information'] as $v):?>
                            <? if($v->file != null):?>
                                <li>
                                    <a href="<?=$v->getFile();?>" target="_blank">
                                        <?=$v->name;?>
                                    </a>
                                </li>
                            <? else:?>
                                <li>
                                    <a href="#"><?=$v->name;?></a>
                                </li>
                            <? endif;?>
                        <? endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer_bottom">
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="col-sm-6">
                <div class="footer-text">
                    <p><?= Yii::$app->view->params['logo']->copyright; ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="footer-text">
                    <p><a href="https://www.a-lux.kz/">Разработка сайтов в Алматы</a></p>
                </div>
            </div>
        </div>
    </div>
</div>


<script defer>
    if (!document.querySelector(".owl-stage").childElementCount) {
        document.querySelectorAll(".owl-prev, .owl-next").forEach((btn) => {
            btn.style.display = "none";
        });
    }
</script>