<?

use yii\helpers\Url;
use yii\widgets\LinkPager;


?>



<div class="main">
    <div class="desktop-version serch-page">
        <div class="container">
            <div class="row">
                <div class="row-list" style="margin-bottom: 30px;margin-top: 30px; width: 100%; justify-content: center;">
                    <div class="title gradient-text">
                        <?php
                        if ($count) { ?>
                            <div style="font-size: 16px;">Результаты поиска по запросу <span style="font-weight: bold;"><?= $keyword ?></span></div>
                        <? } else { ?>
                            <div class="" style="font-size: 24px;">
                                По запросу <span style="font-weight: bold;"><?= $keyword ?></span> ничего не найдено. <br>
                                Возможно, вы набрали неверное название товара
                            </div>
                        <? } ?>
                    </div>
                </div>
                <? if ($count) : ?>
                    <? if(!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)):?>

                        <div class="col-lg-9 col-md-6 col-sm-12">
                            <div class="tablet-version-hide row wow fadeInUp">
                                <? $m = 0;?>
                                <? foreach ($product as $v) : ?>
                                    <? $m++;?>
                                    <? if($m <= 6):?>
                                    <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-4 ']);?>
                                    <? endif;?>
                                <? endforeach; ?>
                            </div>
                        </div>

                        <? if(!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)):?>
                            <div class="col-lg-3 searchbanner">
                                <img src="<?=$banner->getImage();?>" alt="">
                            </div>
                        <? endif;?>


                        <div class="col-lg-12 col-md-6 col-sm-12">
                            <div class="tablet-version-hide row wow fadeInUp">
                                <? $m = 0;?>
                                <? foreach ($product as $v) : ?>
                                    <? $m++;?>
                                    <? if($m > 6):?>
                                        <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-3']);?>
                                    <? endif;?>
                                <? endforeach; ?>
                            </div>
                        </div>

                    <? else:?>

                        <div class="col-lg-12 col-md-6 col-sm-12">

                            <div class="tablet-version-hide row wow fadeInUp">
                                <? foreach ($product as $v) : ?>
                                    <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-3']);?>
                                <? endforeach; ?>
                            </div>
                        </div>

                    <? endif;?>

                    <?= LinkPager::widget([
                        'pagination' => $pagination,
                        'maxButtonCount' => 10
                    ]);
                    ?>

                    <div class="col-lg-12">
                        <div class="result_pagination">
                            <? $pageSizeWeb = (isset($_GET['page']) && $_GET['page'] != 1) ? (($_GET['page']-1) * $pageSize + 14) : count($product);?>
                            <h3>Результаты: <?=$pageSizeWeb;?> из <?=$pagination->totalCount?></h3>
                        </div>
                    </div>

                <? endif; ?>

                <div class="tablet-version col-sm-12">
                    <? if ($product != null) : ?>
                        <div class="row">


                            <? foreach ($product as $v) : ?>
                                <?=$this->render('/partials/desktop_product', ['product' => $v, 'class' => 'col-sm-12 col-md-12 col-lg-4 mb-4']);?>
                            <? endforeach; ?>

                        </div>

                        <?= LinkPager::widget([
                            'pagination' => $pagination,
                            'maxButtonCount' => 10
                        ]);
                        ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>


    <div class="mobile-version mobile-version-search">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="row-list" style="margin-bottom: 30px; margin-top: 10px; display: flex; justify-content: center;">
                            <div class="title gradient-text">
                                <?php
                                if ($count) { ?>
                                    <div style="font-size: 16px; text-align: center">Результаты поиска по запросу </br> <span style="font-weight: bold;"><?= $keyword ?></span></div>
                                <? } else { ?>
                                    <div style="font-size:16px;">
                                        По запросу <span style="font-weight: bold;"><?= $keyword ?></span> ничего не найдено
                                        <br>
                                        Возможно, вы набрали неверное название товара. Поставить точку после каждого предложения
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                        <div class="tab-content tablet-version-hide-mob items">
                            <div class="tab-item tab-item-active" id="home">
                                <div class="row">
                                    <? if ($product != null) : ?>
                                        <? foreach ($product as $v): ?>
                                            <?=$this->render('/partials/mobile_product', ['product' => $v, 'class' => 'col-lg-3 col-md-6 col-sm-12 col-6 mob_p wow fadeInUp']);?>
                                        <?endforeach; ?>

                                        <?= LinkPager::widget([
                                            'pagination' => $pagination,
                                            'maxButtonCount' => 7
                                        ]);
                                        ?>

                                        <div class="col-lg-12">
                                            <div class="result_pagination">
                                                <? $pageSizeMobile = isset($_GET['page']) ? ($_GET['page'] * $pageSize) : count($product);?>
                                                <h3>Результаты: <?=$pageSizeMobile;?> из <?=$pagination->totalCount;?></h3>
                                            </div>
                                        </div>
                                    <? endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    /* ===========
    pagination
    ==============*/
    .pagination {
        padding-left: 0;
        margin: 0 0 50px;
        border-radius: 4px;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: 5px;
        line-height: 1.42857143;
        color: #333;
        text-decoration: none;
        background-color: #fff;
        border: 0;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        font-weight: 600;

    }

    .pagination>li:first-child>a,
    .pagination>li:first-child>span {
        margin-left: 0;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }

    .pagination>li>a:focus,
    .pagination>li>a:hover,
    .pagination>li>span:focus,
    .pagination>li>span:hover {
        z-index: 2;
        color: #ffffff;
        background-color: #68c316;
    }

    .pagination>li:last-child>a,
    .pagination>li:last-child>span {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
        z-index: 3;
        color: #fff;
        background-color: #68c316;
    }
</style>