<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.06.2019
 * Time: 11:26
 */
?>
<?foreach($filial as $v):?>
    <div class="col-sm-6">
        <div class="drugstore-wrap">
            <div class="drugstore-item">
                <p><img src="/images/loc-icon.png" alt="">
                    <b><?= $v->name; ?>, <?=$v->address;?></b></p>
                <? try {
                    $from_time = new DateTime($v->from_time);
                    $to_time = new DateTime($v->to_time);
                } catch (Exception $e) {
                } ?>
                <p><img src="/images/clock.png" alt=""><?=$from_time->format('H:i');?> - <?=$to_time->format('H:i');?> ч.</p>
                <p><img src="/images/phone-icon.png" alt="">
                    <a href="tel:<?= $v->telephone; ?>"><?= $v->telephone; ?></a></p>
                <? if($v->currentStatus == 'Открыто'):?>
                    <div class="status-open">
                        <?=$v->currentStatus;?>
                    </div>
                <? endif?>
                <? if($v->currentStatus == 'Закрыто'):?>
                    <div class="status-close">
                        <?=$v->currentStatus;?>
                    </div>
                <? endif?>
            </div>
        </div>
    </div>
<? endforeach;?>
