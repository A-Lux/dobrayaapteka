<?php

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;


?>

<div class="main">
    <? if(!isMobile()):?>
    <div class="desktop-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="register-title text-center gradient-text wow fadeInDown">
                            <h5>Регистрация</h5>
                        </div>
                        <div class="register-by  gradient-text wow fadeInDown">
                            <p>Регистрация c помощью:</p>
                            <?= yii\authclient\widgets\AuthChoice::widget([
                                'baseAuthUrl' => ['account/auth'],
                                'popupMode' => false,
                            ]) ?>
                        </div>
                    </div>
                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <?php $form = ActiveForm::begin(); ?>

                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="ФИО" name="UserProfile[fio]" value="<?=Yii::$app->session['fio'];?>">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/msg-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Почта" name="SignupForm[email]" value="<?=Yii::$app->session['email'];?>">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/phone-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Номер" name="SignupForm[username]" class="telephone" value="<?=Yii::$app->session['username'];?>">
                                        <script>
                                            $('input[name="SignupForm[username]"]').inputmask("8(999) 999-9999");
                                        </script>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/loc-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Алматы, улица Зауыт, 1Б" name="UserAddress[address][]"
                                               value="<?=Yii::$app->session['address'];?>" class="searcherrr">

                                    </div>
                                    <div class="field_wrapper">
                                        <div>

                                        </div>
                                        <div class="add-address gradient-text">
                                            <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="capcha-img">{image}<br /><div id="refresh-captcha" class="btn btn-ticked add_sl_per">Обновить</div></div>',
                                        'imageOptions' => [
                                            'id' => 'my-captcha-image'
                                        ]
                                    ])->label(false)?>

                                    <?php $this->registerJs("
                                        $('body').on('click', '#refresh-captcha', function(e){
                                            e.preventDefault();
                                            $('#my-captcha-image').yiiCaptcha('refresh');
                                        });
                                    "); ?>

                                    <div class="personal-input">
                                        <input type="text" placeholder="Введите код с картинки" name="SignupForm[verifyCode]">
                                    </div>
                                    <div class="review-btn">
                                        <button type="button" class="register-button">Зарегистрироваться</button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInput" placeholder="Пароль" name="SignupForm[password]" value="<?=Yii::$app->session['password'];?>">
                                        <button type="button" onclick="showPass()"><img src="/images/show-icon.png" alt=""></button>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInput2" placeholder="Повторить пароль" name="SignupForm[password_verify]" value="<?=Yii::$app->session['password_verify'];?>">
                                        <button type="button" onclick="showPass2()"><img src="/images/show-icon.png" alt=""></button>
                                    </div>
                                </div>
<!--                                <div class="col-sm-12">-->
<!--                                    <div class="register-bonus">-->
<!--                                        <p>Начисление бонусов при регистрации</p>-->
<!--                                        <p><span class="gradient-text">500</span> тг!</p>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <? else:?>
    <div class="mobile-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInDown">
                        <div class="register-title text-center gradient-text">
                            <h5>Регистрация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Регистрация c помощью:  </p>
                            <div class="row">
                                <?= yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['account/auth'],
                                    'popupMode' => false,
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <?php $form = ActiveForm::begin(); ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="ФИО" name="UserProfile[fio]" value="<?=Yii::$app->session['fio'];?>">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/msg-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Почта" name="SignupForm[email]" value="<?=Yii::$app->session['email'];?>">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/phone-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Номер" name="SignupForm[username]" class="telephone-mobile" value="<?=Yii::$app->session['username'];?>">
                                        <script>
                                            $('input[name="SignupForm[username]"]').inputmask("8(999) 999-9999");
                                        </script>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/loc-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Алматы, улица Зауыт, 1Б" name="UserAddress[address][]" value="<?=Yii::$app->session['address'];?>">

                                    </div>
                                    <div class="field_wrapper">
                                        <div>

                                        </div>
                                        <div class="add-address gradient-text">
                                            <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInputm" placeholder="Пароль" name="SignupForm[password]" value="<?=Yii::$app->session['password'];?>">
                                        <button  onclick="showPassm()" type="button"><img src="/images/show-icon.png" alt=""></button>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password"  id="myInput2m" placeholder="Повторить пароль" name="SignupForm[password_verify]" value="<?=Yii::$app->session['password_verify']?>">
                                        <button onclick="showPassm2()" type="button"><img src="/images/show-icon.png" alt=""></button>
                                    </div>

                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="capcha">{image}<br />
                                                            <div id="refresh-captcha-mobile" class="btn btn-ticked add_sl_per">Обновить
                                                            </div>
                                                        </div>
                                                        <div class="personal-input">
                                                            <div class="icon">
                                                                <img src="/images/lock-icon.png" alt="">
                                                            </div>
                                                            {input}
                                                        </div>',
                                        'imageOptions' => [
                                            'id' => 'my-captcha-image-mobile'
                                        ]
                                    ])->label(false)?>

                                    <?php $this->registerJs("
                                        $('body').on('click', '#refresh-captcha-mobile', function(e){
                                            e.preventDefault();
                                            $('#my-captcha-image-mobile').yiiCaptcha('refresh');
                                        });
                                    "); ?>

                                    <div class="review-btn">
                                        <button type="button" class="register-button-mob">Зарегистрироваться</button>
                                    </div>
                                </div>
<!--                                <div class="col-sm-12">-->
<!--                                    <div class="register-bonus">-->
<!--                                        <p>Начисление бонусов при регистрации</p>-->
<!--                                        <p><span class="gradient-text">500</span> тг!</p>-->
<!--                                    </div>-->
<!--                                </div>-->

                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? endif;?>
</div>

</div>
<div id="map" style="width: 0; height: 0; float:left;"></div>
<script>


    // $('input[name="UserAddress[address][]"]').inputmask({ mask: "Алматы, i{0,}",
    //     definitions: {
    //         "i": {
    //             validator: ".",
    //             definitionSymbol: "i"
    //         }
    //     },});

    ymaps.ready(init);
    function init() {
        var myPlacemark, myMap = new ymaps.Map("map", {
            center: [43.238293, 76.945465],
            zoom: 11,
            controls: ['zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        });

        var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
        myGeocoder.then(
            function(res) {
                var street = res.geoObjects.get(0);
                var coords = street.geometry.getCoordinates();
                myMap.setCenter(coords);
            },
            function(err) {

            }
        );

        $(`input[name="UserAddress[address][]"]`).autocomplete({
            source: function(request, response) {
                var address = request.term;
                var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>, " + address);
                myGeocoder.then(
                    function(res) {
                        var coords = res.geoObjects.get(0).geometry.getCoordinates();
                        var myGeocoder = ymaps.geocode(coords, {
                            kind: 'house'
                        });
                        myGeocoder.then(
                            function(res) {
                                var myGeoObjects = res.geoObjects;
                                var addresses = [];
                                myGeoObjects.each(function(el, i) {
                                    var arr = [];
                                    arr['title'] = el.properties.get('name');
                                    arr['value'] = "<?= Yii::$app->session["city_name"]; ?>, " + el.properties.get('name');
                                    arr['coords'] = el.geometry.getCoordinates();
                                    addresses.push(arr);
                                });

                                response($.map(addresses, function(address) {
                                    return {
                                        label: address.title,
                                        value: address.value,
                                        coords: address.coords
                                    }
                                }));
                            }
                            );
                    },
                    function(err) {
                        alert('Ошибка');
                    }
                    );
            },
            minLength: 2,
            mustMatch: true
        });


    }
</script>
