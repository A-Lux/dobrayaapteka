<div class="order">
    <div class="container">
        <div class="row mb-4">
            <div class="col-sm-3">
                <a class="get-back-to-orders" href="/account/?tab=orders">Вернуться назад</a>
            </div>
        </div>
        <div class="row">
            <? foreach ($products as $product) : ?>
                <? if($product->product != null):?>
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="order-content">
                        <h4><?= $product->product->name; ?></h4>
                        <br>
                        <p>Цена: <span><?= $product->product->getCalculatePrice(); ?> тг</span></p>
                        <hr>
                        <p>Количество: <span><?= $product->count; ?></span></p>
                        <hr>
                        <p>Общая цена: <span><?= $product->count * $product->product->getCalculatePrice(); ?> тг</span></p>
                        <hr>
                        <a href="/product/<?= $product->product->url; ?>" class="btn-order">Подробнее</a>
                    </div>
                </div>
                <? endif;?>
            <? endforeach; ?>
            <? foreach ($gifts as $product) : ?>
                <? if($product->product != null):?>
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="order-content">
                        <h4><?= $product->product->name; ?></h4>
                        <br>
                        <p style="position: center;">Подарок</p>
                        <hr>
                        <p>Количество: <span><?= $product->count; ?></span></p>
                        <hr>
                        <a href="/product/<?= $product->product->url; ?>" class="btn-order">Подробнее</a>
                    </div>
                </div>
                <? endif;?>
            <? endforeach; ?>
        </div>
    </div>
</div>
