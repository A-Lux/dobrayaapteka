<div class="articles">
    <div class="container">
        <div class="breadcrumb-inner">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Блог</li>
                </ol>
            </nav>
        </div>
        <div class="row" style="margin-top: 20px;margin-bottom: 5px;">
            <div class="col-sm-12 wow slideInLeft">
                <div class="product-title gradient-text">
                    <h5>Блог</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <? foreach ($model as $v):?>
            <div class="col-sm-12 col-md-4 mb-4">
                <a href="/news/<?=$v->url;?>" class="read-more">
                    <img src="<?=$v->getImage();?>" width="300">
                </a>
            </div>
            <div class="col-sm-12 col-md-8 mb-4">
                <a href="/news/<?=$v->url;?>" class="read-more">
                    <h3><?=$v->name;?></h3>
                </a>
                <?=$v->short_description;?>
                <a href="/news/<?=$v->url;?>" class="read-more">Подробнее</a>
            </div>
            <? endforeach;?>
        </div>
    </div>
</div>