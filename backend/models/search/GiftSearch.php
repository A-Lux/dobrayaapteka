<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Gift;

class GiftSearch extends Gift
{
    public function rules()
    {
        return [
            [['id', 'type', 'product_id', 'from_price','to_price'], 'integer'],
            [['products'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Gift::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'product_id' => $this->product_id,
            'from_price' => $this->from_price,
            'to_price' => $this->to_price,
        ]);

        $query->andFilterWhere(['like', 'products', $this->products]);

        return $dataProvider;
    }
}
