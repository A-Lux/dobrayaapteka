<?php

namespace backend\models\search;

use common\models\AdsProducts;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class AdsProductsSearch extends AdsProducts
{
    public function rules()
    {
        return [
            [['id', 'category_id', 'product_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = AdsProducts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'            => $this->id,
            'category_id'   => $this->category_id,
            'product_id'    => $this->product_id,
        ]);

        return $dataProvider;
    }
}
