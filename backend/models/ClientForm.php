<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22.05.2020
 * Time: 13:52
 */

namespace backend\models;


use yii\base\Model;

class ClientForm extends Model
{


    public $password;
    public $username;

    public function rules()
    {
        return [

            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass'=>'common\models\User'],

            [['username'], 'required'],
            [['username'], 'unique', 'targetClass'=>'common\models\User'],

            [['password'], 'required'],
            [['password'], 'string', 'min' => 6],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Пароль',
            'username' => 'Логин',
            'created_at' => 'Дата создания',
        ];
    }


    public function signUp()
    {
        if (!$this->validate()) {
            return null;
        }


        $user = new User();
        $user->username = $this->username;
        $user->role = 2;
        $user->generateAuthKey();
        $user->setPassword($this->password);
        return $user->save(false) ? $user : null;
    }
}
