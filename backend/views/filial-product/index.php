<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Filial Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filial-product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать Filial Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'filial_id',
                'filter'=>\common\models\Filial::getList(),
                'value'=>function($model){
                    return $model->filialName;
                },
            ],
            'product_id',
            'amount',

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
