<?php

use yii\helpers\Html;

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Рекламная продукция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advantages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
    ]) ?>

</div>
