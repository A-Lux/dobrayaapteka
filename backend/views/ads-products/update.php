<?php

use yii\helpers\Html;

$this->title = 'Редактирование "Рекламная продукция": ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Рекламная продукция', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
        'products'  => $products,
    ]) ?>

</div>
