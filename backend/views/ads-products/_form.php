<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="banner-form" style="padding-bottom: 1500px;">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <div class="form-group field-products-category_id required has-error">
                    <label class="control-label" for="products-category_id">Category ID</label>

                    <select name="AdsProducts[category_id]" class="form-control">
                        <option></option>
                        <?
                        $catalog = \common\models\CatalogProducts::find()
                            ->where("level = 1")->all();
                        $array = [];
                        foreach ($catalog as $v){
                            echo '<option value="'.$v->id.'" >'.$v->name.'</option>';
                            $array[$v->id] = $v->name;
                            if(isset($v->childs)){
                                foreach($v->childs as $child1) {
                                    if(!empty($child1->childs))
                                        echo '<option value="'.$child1->id.'" >--'.$child1->name.'</option>';
                                    else{
                                        $selected = '';
                                        if($child1->id == $model->category_id)
                                            $selected = 'selected="selected"';
                                        echo '<option '.$selected.' value="'.$child1->id.'">--'.$child1->name.'</option>';
                                    }
                                    $array[$child1->id] = '--'.$child1->name;
                                    if(isset($child1->childs)){
                                        foreach($child1->childs as $child2){
                                            $selected = '';
                                            if($child2->id == $model->category_id)
                                                $selected = 'selected="selected"';
                                            echo '<option '.$selected.' value="'.$child2->id.'">----'.$child2->name.'</option>';
                                            $array[$child2->id] = '----'.$child2->name;
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>

                </div>

                <div class="form-group">
                    <label> Продукты</label>
                    <select class="form-control" id="ads-products" multiple="multiple" data-placeholder="Выберите продукта"
                            style="width: 100%;" name="products[]">
                        <? if(!$model->isNewRecord): ?>
                            <? if(count($products) > 0):?>
                                <? foreach ($products as $v):?>
                                    <? if($v->product != null):?>
                                        <option value="<?=$v->product->id;?>" selected><?=$v->product->name;?></option>
                                    <? endif;?>
                                <? endforeach;?>
                            <? endif;?>
                        <? endif; ?>
                    </select>
                </div>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
