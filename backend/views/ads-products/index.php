<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Рекламная продукция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advantages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider'  => $dataProvider,
        'filterModel'   => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['/catalog-products/view', 'url' => $model->category->url]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/products/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn'
            ]
        ],
    ]); ?>
</div>
