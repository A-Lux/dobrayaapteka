<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Скидка для пенсионеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-earliest-persons-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'raw',
                'attribute' => 'age',
                'value' => function($data){
                    return $data->age. ' лет';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'discount',
                'value' => function($data){
                    return $data->discount. '%';
                }
            ],
        ],
    ]) ?>

</div>
