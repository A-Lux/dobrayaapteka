<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="//yastatic.net/jquery/1.8.3/jquery.min.js"></script>
<div id="map" style="width: 80%; height: 600px;"></div>
<div id="viewContainer"></div>
<script>
    ymaps.ready(init);

    var coords = [];
    var id_poligon = 0;

    function init() {
        var myMap = new ymaps.Map("map", {
            center: [43.238293, 76.945465],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });

        <?foreach($model as $v){?>
        <?$array = unserialize($v->coords);?>
        var coords = [];
        <?foreach($array[0] as $arr){?>
        coords.push([<?=$arr[0]?>, <?=$arr[1]?>]);
        <?}?>
        var myPolygon<?=$v->id?> = new ymaps.Polygon([coords], {}, {
            editorDrawingCursor: "crosshair",
            fillColor: '#00FF00',
            strokeColor: '#0000FF',
            strokeWidth: 5
        });
        myMap.geoObjects.add(myPolygon<?=$v->id?>);

        //Редактирование
        myPolygon<?=$v->id?>.events.add('click', function () {
            var stateMonitor<?=$v->id?> = new ymaps.Monitor(myPolygon<?=$v->id?>.editor.state);
            stateMonitor<?=$v->id?>.add("drawing", function (newValue) {
                myPolygon<?=$v->id?>.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
            });

            myPolygon<?=$v->id?>.editor.startDrawing();
            myPolygon<?=$v->id?>.editor.startEditing();

            id_poligon = <?=$v->id?>;
            $('.summ').val(<?=$v->summ?>);
            $('.name').val(<?=$v->name?>);

            myPolygon<?=$v->id?>.editor.events.add(['vertexadd'], function(e){
                coords = myPolygon<?=$v->id?>.geometry.getCoordinates();
            });
        });
        <?}?>

        var myPolygon = new ymaps.Polygon([], {}, {
            // Курсор в режиме добавления новых вершин.
            editorDrawingCursor: "crosshair",
            // Максимально допустимое количество вершин.
            //editorMaxPoints: 5,
            // Цвет заливки.
            fillColor: '#00FF00',
            // Цвет обводки.
            strokeColor: '#0000FF',
            // Ширина обводки.
            strokeWidth: 5
        });

        myMap.geoObjects.add(myPolygon);


        $('body').on('click', '.save_geo', function () {
            var sum = $('.summ').val();
            var delivery_time =  $('.name').val();
            if(sum == "") sweetAlert("Ошибка!", "Необходимо заполнить «Цена доставки»", "error");
            else if(delivery_time == "") sweetAlert("Ошибка!", "Необходимо заполнить «Время доставки»", "error");
            else {
                $.ajax({
                    url: '/admin/geocoords/update_geo',
                    method: 'GET',
                    data: {coords: coords, id: id_poligon, summ: sum, name: delivery_time},
                    success: function (response) {
                        $('.create_geoobject').html(response);
                        $('.summ').val('');
                        $('.name').val('');
                    },
                    error: function () {

                    }
                });
            }
        });
    }
</script>

