<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Geocoords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geocoords-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'summ',
            'name'
        ],
    ]) ?>
    <div id="map" style="width: 100%; height: 85vh; float:left;"></div>

    <script>
        ymaps.ready(init);

        function init() {
            var myPlacemark, myMap = new ymaps.Map("map", {
                center: [43.238293, 76.945465],
                zoom: 10,
                controls: ['zoomControl']
            }, {
                searchControlProvider: 'yandex#search'
            });

            var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
            myGeocoder.then(
                function(res) {
                    var street = res.geoObjects.get(0);
                    var coords = street.geometry.getCoordinates();
                    myMap.setCenter(coords);
                },
                function(err) {

                }
            );



            <? $array = unserialize($model->coords); ?>
            var coords = [];

            <? foreach ($array[0] as $arr) { ?>
                coords.push([<?= $arr[0] ?>, <?= $arr[1] ?>]);
            <? } ?>

            var myPolygon = new ymaps.Polygon([coords], {
                // Описываем свойства геообъекта.
                // Содержимое балуна.
                hintContent: "Адрес доставки"
            }, {
                // Задаем опции геообъекта.
                // Цвет заливки.
                fillColor: '#00FF0088',
            });

            myMap.geoObjects.add(myPolygon);
            var myColumn;
            myPolygon.events.add('click', function(e) {
                myMap.geoObjects.remove(myColumn);
                var coords = e.get('coords');
                placemark(myMap, coords);

            });


            function placemark(myMap, coords) {
                myColumn = new ymaps.Placemark(coords, {
                    balloonContent: ''
                }, {
                    preset: 'islands#icon',
                    iconColor: '#0095b6',
                    strokeWidth: 0
                });
                myMap.geoObjects.add(myColumn);

                // Если метка уже создана – просто передвигаем ее.
                if (myPlacemark) {
                    myPlacemark.geometry.setCoordinates(coords);
                }
                // Если нет – создаем.
                else {
                    myPlacemark = createPlacemark(coords);
                    myMap.geoObjects.add(myPlacemark);
                    // Слушаем событие окончания перетаскивания на метке.
                    myPlacemark.events.add('dragend', function() {
                        getAddress(myPlacemark.geometry.getCoordinates());
                    });
                }
                getAddress(coords);
            }


            // Создание метки.
            function createPlacemark(coords) {
                return new ymaps.Placemark(coords, {
                    iconCaption: 'поиск...'
                }, {
                    preset: 'islands#violetDotIconWithCaption',
                    draggable: true
                });
            }
            // Определяем адрес по координатам (обратное геокодирование).
            function getAddress(coords) {
                myPlacemark.properties.set('iconCaption', 'поиск...');
                ymaps.geocode(coords).then(function(res) {
                    var firstGeoObject = res.geoObjects.get(0);

                    myPlacemark.properties
                        .set({
                            // Формируем строку с данными об объекте.
                            iconCaption: [
                                // Название населенного пункта или вышестоящее административно-территориальное образование.
                                firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                                // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                                firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                            ].filter(Boolean).join(', '),
                            // В качестве контента балуна задаем строку с адресом объекта.
                            balloonContent: firstGeoObject.getAddressLine()
                        });
                });
            }



        }




    </script

</div>
