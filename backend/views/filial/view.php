<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->address;
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filial-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'address',
                'value' => $model->address,
                'format' => 'raw',
            ],

            [
                'attribute' => 'status',
                'filter' =>  \backend\controllers\LabelFilial::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelFilial::statusLabel($model->status);
                },
                'format' => 'raw',
            ],

            'from_time',
            'to_time',
            [
                'attribute' => 'telephone',
                'value' => $model->telephone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'longitude',
                'value' => $model->longitude,
                'format' => 'raw',
            ],
            [
                'attribute' => 'latitude',
                'value' => $model->latitude,
                'format' => 'raw',
            ],
            [
                'attribute' => 'city_id',
                'value' => $model->city->name,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
