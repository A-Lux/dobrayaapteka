<?php

use yii\helpers\Html;

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
