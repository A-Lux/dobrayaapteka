<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="review-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($profile, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'date_of_birth')->textInput(['maxlength' => true, 'type' => 'date']) ?>

    <?= $form->field($address, 'address')->textInput(['maxlength' => true]) ?>

    <div class="form-group field-user-password required">
        <label class="control-label" for="user-password">Пароль</label>
        <input type="text" id="user-password" class="form-control" name="User[password]"
               placeholder="Для сохранения того же значения оставьте поле пустым" aria-required="false">
        <div class="help-block"></div>

    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
