<aside class="main-sidebar">

    <div class="user-panel">
        <div class="pull-left image">
            <img src="/backend/web/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?=Yii::$app->user->identity->username;?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
    </ul>


    <?


    $user = null;$page = null;$mainsub = null;$banner = null;
    $advantages = null;$main = null;$products_inf = null;$news = null;
    $trigger = null;$orders_inf = null;$contact = null;$logo = null;$text = null;
    $message = null;$email = null;$sms_phones = null;$orders = null; $filial = null;
    $catalog = null;$products = null;$country = null;$geocoords = null;
    $promocode = null; $gift = null;$discount= null;$free_delivery = null;$promotional_products	= null;
    $birthday = null; $holiday = null;$tovar_month = null; $remind_basket = null; $clients = null;
    $legal_information = null; $sliders = null;$search_banner = null;$search = null;

    if (Yii::$app->user->identity->role == 1) {
        $user = ['label' => 'Доступы', 'icon' => 'fa fa-user', 'url' => ['/user'],'active' => $this->context->id == 'user'];}
    if (Yii::$app->view->params['admission']->clients) {
        $clients = ['label' => 'Пользователей', 'icon' => 'fa fa-user', 'url' => ['/client'],'active' => $this->context->id == 'client'];}
    if (Yii::$app->view->params['admission']->menu) {
        $page = ['label' => 'Страницы', 'icon' => 'fa fa-user', 'url' => ['/menu'],'active' => $this->context->id == 'menu'];}

    $check = false;
    if (Yii::$app->view->params['admission']->mainsub) {
        $check = true;
        $mainsub = ['label' => 'Заголовок страницы', 'icon' => 'fa fa-user', 'url' => ['/mainsub'], 'active' => $this->context->id == 'mainsub'];}
    if (Yii::$app->view->params['admission']->banner) {
        $check = true;
        $banner =  ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/banner'], 'active' => $this->context->id == 'banner'];}
    if (Yii::$app->view->params['admission']->sliders) {
        $check = true;
        $sliders = ['label' => 'Нижние баннеры', 'icon' => 'fa fa-user', 'url' => ['/slider'], 'active' => $this->context->id == 'slider'];}
    if (Yii::$app->view->params['admission']->advantages) {
        $check = true;
        $advantages = ['label' => 'Наши преимущества', 'icon' => 'fa fa-user', 'url' => ['/advantages'], 'active' => $this->context->id == 'advantages'];}
    if ($check) {
        $main = ['label' => 'Главная', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$mainsub,$banner,$sliders,$advantages]];}



    $check = false;
    if (Yii::$app->view->params['admission']->filial) {
        $check = true;
        $filial = ['label' => 'Филиалы', 'icon' => 'fa fa-user', 'url' => ['/filial'],'active' => $this->context->id == 'filial'];}
    if (Yii::$app->view->params['admission']->catalog) {
        $check = true;
        $catalog =  ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/catalog-products'],'active' => $this->context->id == 'catalog-products'];}
    if (Yii::$app->view->params['admission']->products) {
        $products = ['label' => 'Продукция ', 'icon' => 'fa fa-user', 'url' => ['/products'],'active' => $this->context->id == 'products'];}
    if (Yii::$app->view->params['admission']->country) {
        $check = true;
        $country = ['label' => 'Страны', 'icon' => 'fa fa-user', 'url' => ['/country'], 'active' => $this->context->id == 'country'];}
    if (Yii::$app->view->params['admission']->geocoords) {
        $check = true;
        $geocoords = ['label' => 'Координаты', 'icon' => 'fa fa-user', 'url' => ['/geocoords'],'active' => $this->context->id == 'geocoords'];}
    if (Yii::$app->view->params['admission']->promocode) {
        $check = true;
        $promocode = ['label' => 'Промокоды', 'icon' => 'fa fa-user', 'url' => ['/promo-code'],'active' => $this->context->id == 'promo-code'];}
    if (Yii::$app->view->params['admission']->gift) {
        $check = true;
        $gift = ['label' => 'Подарки', 'icon' => 'fa fa-user', 'url' => ['/gift'],'active' => $this->context->id == 'gift'];}
    if (Yii::$app->view->params['admission']->discount) {
        $check = true;
        $discount = ['label' => 'Скидка для пенсионеров', 'icon' => 'fa fa-user', 'url' => ['/discount-earliest-persons'],'active' => $this->context->id == 'discount-earliest-persons'];}
    if (Yii::$app->view->params['admission']->free_delivery) {
        $check = true;
        $free_delivery = ['label' => 'Бесплатная доставка', 'icon' => 'fa fa-user', 'url' => ['/price-free-delivery'],'active' => $this->context->id == 'price-free-delivery'];}

    if (Yii::$app->view->params['admission']->promotional_products) {
        $check = true;
        $promotional_products = ['label' => 'Рекламная продукция', 'icon' => 'fa fa-user', 'url' => ['/ads-products'],'active' => $this->context->id == 'ads-products'];}

    if($check){
        $products_inf = ['label' => 'Продукция', 'icon' => 'fa fa-user', 'url' => '#', 'items' => [$filial, $catalog, $products, $country,
            $geocoords, $promocode, $gift, $discount, $free_delivery, $promotional_products]];}


    $check = false;
    if (Yii::$app->view->params['admission']->orders) {
        $check = true;
        $orders = ['label' => 'Заказы', 'icon' => 'fa fa-user', 'url' => ['/orders'], 'active' => $this->context->id == 'orders'];}
    if (Yii::$app->view->params['admission']->sms_notification) {
        $check = true;
        $sms_phones =  ['label' => 'Список рассылки', 'icon' => 'fa fa-user', 'url' => ['/sms-notification-lists'], 'active' => $this->context->id == 'sms-notification-lists'];}
    if ($check) {
        $orders_inf = ['label' => 'Заказы', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$orders, $sms_phones]];}


    $check = false;
    if (Yii::$app->view->params['admission']->trigger_birthday) {
        $check = true;
        $birthday = ['label' => 'В дни рождение ', 'icon' => 'fa fa-user', 'url' => ['/trigger-birthday'],'active' => $this->context->id == 'trigger-birthday'];}
    if (Yii::$app->view->params['admission']->trigger_holiday) {
        $check = true;
        $holiday = ['label' => 'В празднике ', 'icon' => 'fa fa-user', 'url' => ['/trigger-holiday'],'active' => $this->context->id == 'trigger-holiday'];}
    if (Yii::$app->view->params['admission']->trigger_tovar_month) {
        $check = true;
        $tovar_month= ['label' => 'При обновлении Товара Месяца', 'icon' => 'fa fa-user', 'url' => ['/trigger-tovar-month'],'active' => $this->context->id == 'trigger-tovar-month'];}
    if (Yii::$app->view->params['admission']->trigger_remind_basket) {
        $check = true;
        $remind_basket = ['label' => 'При не осущестивление покупку', 'icon' => 'fa fa-user', 'url' => ['/trigger-remind-basket'],'active' => $this->context->id == 'trigger-remind-basket'];}
    if ($check) {
        $trigger = ['label' => 'Триггерные письма', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$birthday,$holiday,$tovar_month,$remind_basket]];}



    $check = false;
    if (Yii::$app->view->params['admission']->text) {
        $check = true;
        $text = ['label' => 'По сайту', 'icon' => 'fa fa-user', 'url' => ['/text'], 'active' => $this->context->id == 'text'];}
    if (Yii::$app->view->params['admission']->messages) {
        $check = true;
        $message =  ['label' => 'Сообщения', 'icon' => 'fa fa-user', 'url' => ['/message'], 'active' => $this->context->id == 'message'];}
    if ($check) {
        $message = ['label' => 'Тексты', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$text, $message]];}

    $check = false;
    if(isset(Yii::$app->view->params['admission']->search_banner)){
        if (Yii::$app->view->params['admission']->search_banner) {
            $check = true;
            $search_banner = ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/search-banner'], 'active' => $this->context->id == 'search-banner'];}    
    }
    if ($check) {
        $search = ['label' => 'Поиск', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$search_banner]];}



    if (Yii::$app->view->params['admission']->news) {
        $news = ['label' => 'Новости', 'icon' => 'fa fa-user', 'url' => ['/news'],  'active' => $this->context->id == 'news'];}
    if (Yii::$app->view->params['admission']->contact) {
        $contact = ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/contact'],  'active' => $this->context->id == 'contact'];}
    if (Yii::$app->view->params['admission']->logo) {
        $logo = ['label' => 'Логотип и Копирайт', 'icon' => 'fa fa-user', 'url' => ['/logo'],'active' => $this->context->id == 'logo'];}
    if (Yii::$app->view->params['admission']->email) {
        $email =  ['label' => 'Эл. почта для связи ', 'icon' => 'fa fa-user', 'url' => ['/emailforrequest'],'active' => $this->context->id == 'emailforrequest'];}
    if (Yii::$app->view->params['admission']->legal_information) {
        $legal_information =  ['label' => 'Юридическая информация ', 'icon' => 'fa fa-user', 'url' => ['/legal-information'],'active' => $this->context->id == 'legal-information'];}
    ?>



    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    $user,
                    $clients,
                    $page,
                    $main,
                    $products_inf,
                    $orders_inf,
                    $trigger,
                    $message,
                    $search,
                    $news,
                    $contact,
                    $logo,
                    $email,
                    $legal_information,
                ],
            ]
        ) ?>
    </section>

</aside>
