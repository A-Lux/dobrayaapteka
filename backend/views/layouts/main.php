<?php

use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    AppAsset::register($this);

    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://api-maps.yandex.ru/2.1/?apikey=0b0a8929-e785-47a7-904a-87e8c66e8dcc&lang=ru_RU" type="text/javascript"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="/backend/web/js/clipboard.min.js"></script>


        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper" >

        <?= $this->render(
            '_header.php'
        ) ?>

        <?= $this->render(
            '_left.php'
        )
        ?>

        <?= $this->render(
            '_content.php',
            ['content' => $content]
        ) ?>

    </div>







    <?php $this->endBody() ?>
    </body>


    <script>

        function showSuccessAlert(text){
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true
            });
            Toast.fire({
                icon: 'success',
                title: text
            });
        }

        $.fn.select2.amd.define('select2/i18n/ru',[],function () {
            // Russian
            return {
                errorLoading: function () {
                    return 'Поиск…';
                },
                inputTooLong: function (args) {
                    var overChars = args.input.length - args.maximum;
                    var message = 'Пожалуйста, удалите ' + overChars + ' символ';
                    if (overChars >= 2 && overChars <= 4) {
                        message += 'а';
                    } else if (overChars >= 5) {
                        message += 'ов';
                    }
                    return message;
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;

                    var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';

                    return message;
                },
                loadingMore: function () {
                    return 'Загружаем ещё ресурсы…';
                },
                maximumSelected: function (args) {
                    var message = 'Вы можете выбрать ' + args.maximum + ' элемент';

                    if (args.maximum  >= 2 && args.maximum <= 4) {
                        message += 'а';
                    } else if (args.maximum >= 5) {
                        message += 'ов';
                    }

                    return message;
                },
                noResults: function () {
                    return 'Ничего не найдено';
                },
                searching: function () {
                    return 'Поиск…';
                }
            };
        });


        $(function () {
            //Initialize Select2 Elements
            $('#sop_tovary').select2({
                language: "ru",
                multiple: true,
                tokenSeparators: [',', ' '],
                minimumInputLength: 2,
                minimumResultsForSearch: 10,
                ajax: {
                    url: '/admin/products/search',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        };

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.ware_id
                                }
                            })
                        };
                    }
                }
            });

            $('#analogy').select2({
                language: "ru",
                multiple: true,
                tokenSeparators: [',', ' '],
                minimumInputLength: 2,
                minimumResultsForSearch: 10,
                ajax: {
                    url: '/admin/products/search',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        };

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.ware_id
                                }
                            })
                        };
                    }
                }
            });

            $('#ads-products').select2({
                language: "ru",
                multiple: true,
                tokenSeparators: [',', ' '],
                minimumInputLength: 2,
                minimumResultsForSearch: 10,
                ajax: {
                    url: '/admin/products/search',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        };

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });

            $('#orders').select2({
                language: "ru",
                multiple: true,
                tokenSeparators: [',', ' '],
                minimumInputLength: 2,
                minimumResultsForSearch: 10,
                ajax: {
                    url: '/admin/products/search',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        };

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });

            $('#gift').select2({
                language: "ru",
                multiple: true,
                tokenSeparators: [',', ' '],
                minimumInputLength: 2,
                minimumResultsForSearch: 10,
                ajax: {
                    url: '/admin/products/search',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        };

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });

            $('#gift_product').select2({
                language: "ru",
                multiple: false,
                tokenSeparators: [',', ' '],
                minimumInputLength: 2,
                minimumResultsForSearch: 10,
                ajax: {
                    url: '/admin/products/search',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        };

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });

        })
    </script>

    </html>
    <?php $this->endPage() ?>
<?php } ?>
