<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'subject',
                'value' => function ($model) {
                    return $model->subject;
                },
                'format' => 'raw',
            ],
            'created_at',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
            ],
    ]); ?>
</div>
