<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Нижние баннеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',

            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}']
        ],
    ]); ?>
</div>
