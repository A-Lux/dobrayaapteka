<?php

use yii\helpers\Html;

/* @var $model common\models\Banner */
$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Нижние баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
