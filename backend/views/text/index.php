<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Текст по сайту';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'text',
                'value' => function ($data) {
                    return $data->text;
                },
                'format' => 'raw',
            ],
            [
                'attribute'=>'type_id',
                'filter'=>\common\models\TextType::getList(),
                'value'=>function($model){
                    return $model->type->name;
                },
            ],

            ['class' => 'yii\grid\ActionColumn','template' => '{update} {view}'],
        ],
    ]); ?>
</div>
