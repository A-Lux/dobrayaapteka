<?php

use yii\helpers\Html;

$this->title = 'Редактирование текста: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Текст по сайту', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="text-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
