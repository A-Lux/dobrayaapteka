<?php

use yii\helpers\Html;

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Trigger Holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trigger-holiday-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
