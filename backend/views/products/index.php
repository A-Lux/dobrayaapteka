<?php

use backend\controllers\LabelInStock;
use backend\controllers\LabelNew;
use backend\controllers\LabelPopular;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Продукция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Сортировка (Топ продаж)', ['products/index-hit'], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Сортировка (Товар месяца)', ['products/index-top-month'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                 'attribute' => 'category_id',
                 'filter' => \common\models\CatalogProducts::getCategoryList(),
                 'value' => function ($model) {
                    if($model->category != null){
                        return $model->category->name;
                    }else{
                        return null;
                    }

                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],

            [
                'attribute' => 'name',
                'value' => function ($data) {
                    return $data->name;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => LabelPopular::statusList(),
                'value' => function ($model) {
                    return LabelPopular::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isNew',
                'filter' => LabelNew::statusList(),
                'value' => function ($model) {
                    return LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'images',
                'filter' => \backend\controllers\LabelAvailability::statusList(),
                'value' => function ($model) {

//                    $status = 0;
//                    if($model->images){
//                        $files = unserialize($model->images);
//                        if(file_exists($model->path . $files[0])){
//                             $status = 1;
//                        }
//                    }

                    $status = $model->images != '' ? 1 : 0;
                    return \backend\controllers\LabelAvailability::statusLabel($status);
                },
                'format' => 'raw',
            ],


            [
                'attribute' => 'text',
                'filter' => \backend\controllers\LabelAvailability::statusList(),
                'value' => function ($model) {
                    $status = $model->text != '' ? 1 : 0;
                    return \backend\controllers\LabelAvailability::statusLabel($status);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
