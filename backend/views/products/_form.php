<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

?>

<link rel="stylesheet" src="/backend/web/private/bower_components/select2/dist/css/select2.min.css">

<div class="products-form" style="padding-bottom: 2700px;">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Статусы</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">

                <?= $form->field($model, 'top_month')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

                <?= $form->field($model, 'isNew')->dropDownList([0 => 'Нет',1 => 'Да']) ?>

                <?= $form->field($model, 'isHit')->dropDownList([0 => 'Нет',1 => 'Да'])?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'description')->textarea() ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
                        'options' => ['rows' => 6],
                        'allowedContent' => true,
                        'preset' => 'full',
                        'inline' => false
                    ]),
                ]) ?>

                <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

                <? if($model->images):?>
                    <? $files = unserialize($model->images);?>
                    <? if(file_exists($model->path . $files[0])):?>

                        <div id="images">
                            <div class="row">
                                <div class="" style="text-align: center">
                                    <div style="margin-top:20px;">
                                        <img src="<?=$model->getImage();?>" alt="" style="margin-left:15px;width:400px;height:400px;float:left;">
                                    </div>
                                </div>
                            </div>
                            <button type="button" style="margin-bottom: 30px;margin-top: 10px;" onclick="deleteImage(<?=$model->id;?>)"
                                    class="btn btn-danger">Удалить картинку</button>
                        </div>
                    <? endif;?>
                <? endif;?>


                <?= $form->field($model, 'files[]')->label(false)->widget(FileInput::className(), [
                    'options' => [
                        'accept' => 'image/*',
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'showUpload' => false
                    ],
                ]) ?>
                <div class="form-group field-products-category_id required has-error">
                    <label class="control-label" for="products-category_id">Категория</label>

                    <select name="Products[category_id]" class="form-control">
                        <option></option>
                        <?
                        $catalog = \common\models\CatalogProducts::find()
                            ->where("level = 1")->all();
                        $array = [];
                        foreach ($catalog as $v){
                            echo '<option value="'.$v->id.'" disabled="disabled">'.$v->name.'</option>';
                            $array[$v->id] = $v->name;
                            if(isset($v->childs)){
                                foreach($v->childs as $child1) {
                                    if(!empty($child1->childs))
                                        echo '<option value="'.$child1->id.'" disabled="disabled">--'.$child1->name.'</option>';
                                    else{
                                        $selected = '';
                                        if($child1->id == $model->category_id)
                                            $selected = 'selected="selected"';
                                        echo '<option '.$selected.' value="'.$child1->id.'">--'.$child1->name.'</option>';
                                    }
                                    $array[$child1->id] = '--'.$child1->name;
                                    if(isset($child1->childs)){
                                        foreach($child1->childs as $child2){
                                            $selected = '';
                                            if($child2->id == $model->category_id)
                                                $selected = 'selected="selected"';
                                            echo '<option '.$selected.' value="'.$child2->id.'">----'.$child2->name.'</option>';
                                            $array[$child2->id] = '----'.$child2->name;
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>

                </div>
                <?//= $form->field($model, 'category_id')->dropDownList($array ,['prompt' => '']) ?>
                <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'country')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::getAll(), 'id' ,'name'), ['prompt' => '']) ?>

                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'discount')->textInput(['type' => 'number', 'min' => 0]) ?>

                <?= $form->field($model, 'bonus')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <label> Сопутствующие товары</label>
                    <select class="form-control" id="sop_tovary" multiple="multiple" data-placeholder="Выберите продукта"
                            style="width: 100%;" name="sop_tovary[]">
                        <? if(count($sop_tovary) > 0):?>
                            <? foreach ($sop_tovary as $v):?>
                                <? if($v->product != null):?>
                                    <option value="<?=$v->product->ware_id;?>" selected><?=$v->product->name;?></option>
                                <? endif;?>
                            <? endforeach;?>
                        <? endif;?>
                    </select>
                </div>

                <div class="form-group">
                    <label> Аналоги лекарственных средств</label>
                    <select class="form-control" id="analogy" multiple="multiple" data-placeholder="Выберите продукта"
                            style="width: 100%;" name="analogy[]">
                        <? if(count($analogy) > 0):?>
                            <? foreach ($analogy as $v):?>
                                <? if($v->product != null):?>
                                    <option value="<?=$v->product->ware_id;?>" selected><?=$v->product->name;?></option>
                                <? endif;?>
                            <? endforeach;?>
                        <? endif;?>
                    </select>
                </div>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<script>

    function deleteImage(id) {
        $.ajax({
            type: 'GET',
            url: '/admin/products/delete-image',
            data: {id:id},
            success: function (response) {
                if (response == 1) {
                    $('#images').hide();
                }
            },
            error: function () {
                alert('Что-то пошло не так.');
            }
        });
    }

</script>
