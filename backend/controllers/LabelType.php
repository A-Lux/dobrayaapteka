<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.03.2019
 * Time: 20:29
 */

namespace backend\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelType
{
    public static function statusList()
    {
        return [
            1 => 'По факту',
            2 => 'Онлайн оплата',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 1:
                $class = 'label label-success';
                break;
            case 2:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }




    public static function statusLabelValue($status)
    {
        $class = "";
        switch ($status) {
            case 1:
                $class = 'По факту';
                break;
            case 2:
                $class = 'Онлайн оплата';
                break;
        }

        return $class;
    }
}
