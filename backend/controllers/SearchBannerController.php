<?php

namespace backend\controllers;

use Yii;
use common\models\SearchBanner;
use backend\models\search\SearchBannerSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class SearchBannerController extends BackendController
{
    const my_id = 1;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function afterAction($action, $result)
    {
        if (!Yii::$app->view->params['admission']->search_banner){
            throw new NotFoundHttpException();
        }else {
            return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
        }
    }

    public function actionIndex()
    {
        return $this->render('view', [
            'model' => $this->findModel(self::my_id),
        ]);
    }



    public function actionUpdate()
    {
        $model = $this->findModel(self::my_id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if($oldImage != null){
                    if(file_exists($model->path . $oldImage)) {
                        unlink($model->path . $oldImage);
                    }
                }
            }

            if($model->save()){
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = SearchBanner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
