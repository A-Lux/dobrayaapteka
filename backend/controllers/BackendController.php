<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2019
 * Time: 15:32
 */

namespace backend\controllers;


use common\models\Admission;
use common\models\Products;
use common\models\Review;
use Yii;
use yii\web\Controller;

class BackendController extends Controller
{

    public function beforeAction($action){

        if (Yii::$app->user->isGuest) {
            return $this->redirect('/admin/site/login')->send();
        }elseif(Yii::$app->user->identity->role == null){
            Yii::$app->user->logout();
            return $this->redirect('/admin/site/login')->send();
        }


        Yii::$app->view->params['admission'] = Admission::findOne(["user_id" => Yii::$app->user->id]);

        Yii::$app->view->params['feedback'] = Review::findAll(['isRead' => 0]);

        return parent::beforeAction($action);

    }


}
