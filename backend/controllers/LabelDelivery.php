<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.12.2019
 * Time: 10:48
 */

namespace backend\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelDelivery
{
    public static function statusList()
    {
        return [
            0 => 'Не получено',
            1 => 'Получено',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

    public static function statusLabelValue($status)
    {
        $class = "";
        switch ($status) {
            case 0:
                $class = 'Не доставлено';
                break;
            case 1:
                $class = 'Доставлено';
                break;
        }

        return $class;
    }

}
