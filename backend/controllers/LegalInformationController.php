<?php

namespace backend\controllers;

use common\models\News;
use Yii;
use common\models\LegalInformation;
use backend\models\search\LegalInformationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class LegalInformationController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new LegalInformationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new LegalInformation();
        if ($model->load(Yii::$app->request->post())) {
            $file= UploadedFile::getInstance($model, 'file');
            if($file != null) {
                $file->saveAs($model->path .  $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create',compact('model'));
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFile = $model->file;

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if($file == null){
                $model->file = $oldFile;
            }else{
                $file->saveAs($model->path .  $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;
                if($oldFile != null){
                    if(file_exists($model->path . $oldFile)) {
                        unlink($model->path . $oldFile);
                    }
                }
            }

            if($model->save()){
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {

        $model = LegalInformation::findOne($id);
        $models = LegalInformation::find()->where('sort > '.$model->sort)->all();

        foreach($models as $v){
            $v->sort--;
            $v->save(false);
        }


        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = LegalInformation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMoveUp($id)
    {
        $model = LegalInformation::findOne($id);
        if ($model->sort != 1) {
            $sort = $model->sort - 1;
            $model_down = LegalInformation::find()->where("sort = $sort")->one();
            $model_down->sort += 1;
            $model_down->save(false);

            $model->sort -= 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }

    public function actionMoveDown($id)
    {
        $model = LegalInformation::findOne($id);
        $model_max_sort = LegalInformation::find()->orderBy("sort DESC")->one();

        if ($model->id != $model_max_sort->id) {
            $sort = $model->sort + 1;
            $model_up = LegalInformation::find()->where("sort = $sort")->one();
            $model_up->sort -= 1;
            $model_up->save(false);

            $model->sort += 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }
}
