<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "CatalogProducts".
 *
 * @property int $id
 * @property string $image
 * @property string $mobile_image
 * @property string $title
 * @property string $description
 * @property string $name
 * @property int $sort
 * @property int $status
 * @property int $level
 * @property int $parent_id
 * @property string $created_at
 * @property string $url
 *
 *
 */

class CatalogProducts extends \yii\db\ActiveRecord
{
    public $path = 'images/catalogproducts/';

    public static function tableName()
    {
        return 'catalog_products';
    }

    public function rules()
    {
        return [
            [['id', 'title', 'description', 'name', 'sort', 'status', 'level'], 'required'],
            [['parent_id', 'sort', 'status', 'level'], 'integer'],
            [['description', 'created_at'], 'string'],
            [['title', 'name', 'url'], 'string', 'max' => 255],
            [['image', 'mobile_image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'name' => 'Название',
            'url' => 'Ссылка',
            'image' => 'Иконка для десктопу',
            'mobile_image' => 'Иконка для мобилку',
            'sort' => 'Сортировка',
            'status' => 'Статус',
            'created_at' => 'Дата создание',
        ];
    }


    public static function primaryKey()
    {
        return ['id'];
    }

    public function getAdsProduct()
    {
        return $this->hasMany(AdsProducts::className(), ['category_id' => 'id']);
    }


    public static function getCategoryList(){

        $data = ArrayHelper::map(self::find()
            ->where(['not', ['parent_id' => null]])
            ->orderBy('name', 'ASC')
            ->all(), 'id', 'name');
        $data[1] = '(не задано)';

        return $data;
    }

    public static function getSecondLevelCatalog(){
        return self::find()
            ->where('status = 1 AND level = 1')
            ->limit(6)
            ->with('childs')
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')])
            ->all();
    }


    public static function getAllLevelCatalog(){
        return self::find()
            ->where('status = 1 AND level = 1')
            ->with('childs')
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')])->all();
    }


    public static function getCategories($catalog_id){
        return self::find()
            ->where(['parent_id' => $catalog_id, 'status' => 1])
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')])
            ->all();
    }


    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getChilds()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id'])
            ->where('status = 1')
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')]);
    }

    public static function getFirstLevel(){
        return CatalogProducts::find()
            ->where('status = 1 AND level = 1')
            ->limit(6)
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')])
            ->all();
    }

    public static function getSecondLevel(){
        return CatalogProducts::find()
            ->where('status = 1 AND level = 2')
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')])
            ->all();
    }

    public static function getThirdLevel(){
        return CatalogProducts::find()
            ->where('status = 1 AND level = 3')
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')])
            ->all();
    }


    public static function getProducts($id, $quantity,  $sql=""){

        $ids = self::startRecursive($id, $sql);
        $ids = $ids.$id;
        $products = Products::find()
            ->distinct()
            ->select('products.id, products.url, products.isHit, products.images, products.name, 
            products.price, products.discount, products.top_month, products.status_products, products.sort')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial_product.amount > 0')
            ->andWhere('filial.status = 2');
            
        if ($sql != "") {
            $products = $products
                ->andWhere('products.category_id in (' . $ids .') '. $sql);
        }else{
            $products = $products
                ->andWhere('products.category_id in (' . $ids . ')' );
        }

        $products = $products
            ->offset($quantity)
            ->all();

        return $products;
    }


    public static function getProductMinPrice($id){

        $ids = self::startRecursive($id);
        $ids = $ids.$id;

        return Products::find()
            ->distinct()
            ->select('products.price')
            ->innerJoin('filial_product', '`filial_product`.`product_id` = `products`.`id`')
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial_product.amount > 0')
            ->andWhere('filial.status = 2')
            ->andWhere('products.category_id in (' . $ids . ')' )
            ->min('price');
    }


    private static function startRecursive($id, $sql = null){

        $lvl = self::getAllCatalogByParentID($id);
        $ids = "";
        if($lvl != null) {
            foreach ($lvl as $v) {
                $ids .= $v->id.','.self::startRecursive($v->id, $sql);
            }
        }

        return $ids;
    }



    public static function getAllCatalogByParentID($id){
        return self::findAll(['parent_id' => $id]);
    }

    public static function getLast(){
        return self::find()->orderBy('sort DESC')->one();;
    }



    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = self::getLast();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }




    public function generateCyrillicToLatin(){
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
            ',','.',' ','/','(',')','|','_'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            '-','-','-','-','-','-','-','-'
        ];
        $text = str_replace($cyr,$lat, $this->name);
        if(substr($text, -1) == '-') $text = substr($text, 0, -1);
        return preg_replace('/[^a-zA-Z0-9-]/','', $text).'-'.time();
    }

    public function getDesktopImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path . $this->image : '';
    }

    public function getMobileImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path . $this->mobile_image : '';
    }


    public function getUrl()
    {
        $url = "";
        $url = $this->startRecursiveForUrl($url, $this->parent_id);
        return '/catalog'.$url.'/'.$this->url;
    }


    public function getImage()
    {
        return ($this->img) ? '/uploads/' . $this->img : '/no-image.png';
    }


    private static function startRecursiveForUrl($url, $id){

        $lvl = self::findOne(['id' => $id]);

        if($lvl != null){
            $url .= '/'.$lvl->url;
            return self::startRecursiveForUrl($url, $lvl->parent_id);
        }
        else return $url;

    }

}
