<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "UserGift".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int $quantity
 */

class UserGift extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_gift';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id','quantity'], 'required'],
            [['user_id', 'product_id','quantity'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количества'
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(),['id'=>'product_id']);
    }
}
