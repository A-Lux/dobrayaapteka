<?php
namespace common\models;

use Yii;


/**
 * This is the model class for table "Gift".
 *
 * @property int $id
 * @property int $type
 * @property string $products
 * @property int $from_price
 * @property int $to_price
 * @property int $product_id
 */


class Gift extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'gift';
    }

    public function rules()
    {
        return [
            [['type','products'], 'required'],
            [['type', 'product_id', 'from_price','to_price'], 'integer'],
            [['products'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'product_id' => 'Продукт',
            'from_price' => 'Цена от',
            'to_price' => 'Цена до',
            'products' => 'Подарки',
        ];
    }


    public static function getProducts()
    {
        return \yii\helpers\ArrayHelper::map(Products::find()
            ->innerJoin('gift', '`gift`.`product_id` = `products`.`id`')
            ->all(),'id','name');
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public static function getTypes(){
        return [0 => 'По продуктам', 1 => 'По ценам'];
    }

    public function getTypeName(){
        $gift = $this->types;
        return $gift[$this->type];
    }


    public function getProductName(){
        return $this->product ? $this->product->name : "";
    }


    public function getAvailableProducts(){

        $arr = explode(",", $this->products);
        $availableProducts = "";
        for($i=0;$i<count($arr);$i++){
            $product = Products::findOne(['id' => (int)$arr[$i]]);
            if($product){
                $availableProducts.= $product->name."<br>";
            }
        }

        $availableProducts = substr($availableProducts, 0, -1);

        return $availableProducts;
    }


    public function getAvailableProductsAsArray(){

        $arr = explode(",", $this->products);
        $response = array();
        for($i=0;$i<count($arr);$i++){
            $response[$arr[$i]] = $arr[$i];
        }
        return $response;
    }


    public static function checkGift($sum){

//        $check = 0;
//        // По ценам
//        $gift = Gift::find()->where('type=1 AND (from_price<'.$sum.' AND to_price>'.$sum.')')->one();
//        if($gift){
//            $check++;
//            $arr = '('.$gift->products.')';
//            $products = Products::find()->where('id in '.$arr)->all();
//            $_SESSION['gift_for_price'] = $products;
//            $_SESSION['gift_for_type'] = '1';
//        }
//
//        //По продуктам
//        $gift = Gift::find()->where('type=0')->all();
//        $isEntered = false;
//        $orderProducts = Products::getOrderProductsAsArray();
//        $products = array();
//        foreach ($gift as $v){
//            if($v->product_id == $orderProducts[$v->product_id]){
//                $isEntered = true;
//                $arr = '('.$v->products.')';
//                $product = Products::find()->where('id in '.$arr)->all();
//                foreach ($product as $v){
//                    array_push($products, $v);
//                }
//            }
//        }
//        if($isEntered){
//            $_SESSION['gift_for_product'] = $products;
//            $_SESSION['gift_for_type'] = '0';
//            $check++;
//        }
//
//
//        if($check == 2){
//            $_SESSION['gift_for_type'] = '2';
//            return Orders::haveGift;
//        }elseif($check == 1){
//            return Orders::haveGift;
//        }else{
//            return Orders::isUserSuccessOrdered;
//        }

        return Orders::isUserSuccessOrdered;
    }



}
