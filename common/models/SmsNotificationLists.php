<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "SmsNotificationLists".
 *
 * @property int $id
 * @property int $status
 * @property string $phone
 * @property string $created_at
 * @property string $updated_at
 */

class SmsNotificationLists extends \yii\db\ActiveRecord
{

    const isActive = 1;
    const isNotActive = 0;

    public static function tableName()
    {
        return 'sms_notification_lists';
    }

    public function rules()
    {
        return [
            [['phone', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['phone'], 'string', 'max' => 255],
            [['status'], 'number']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public static function getActivePhones(){
        return self::find()->where(['status' => self::isActive])->all();
    }
}
