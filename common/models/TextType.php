<?php
namespace common\models;

use Yii;

class TextType extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'text_type';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(TextType::find()->all(),'id','name');
    }


}
