<?php
namespace common\models;

use Yii;


/**
 * This is the model class for table "Advantages".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $url
 * @property string $content
 * @property string $image
 */

class Advantages extends \yii\db\ActiveRecord
{

    public $path = 'images/advantages/';

    public static function tableName()
    {
        return 'advantages';
    }

    public function rules()
    {
        return [
            [['name', 'text', 'url', 'metaName'], 'required'],
            [['text','content', 'metaDesc', 'metaKey'], 'string'],
            [['name','url', 'metaName'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'text' => 'Текст',
            'content' => 'Содержание',
            'url' => 'Ссылка',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
        ];
    }

    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public static function getAll(){
        return Advantages::find()->all();
    }


}
