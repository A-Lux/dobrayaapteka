<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Gift".
 *
 * @property int $id
 * @property string $name
 * @property string $file
 * @property int $created_at
 * @property int $updated_at
 * @property int $sort
 */

class LegalInformation extends \yii\db\ActiveRecord
{

    public $path = 'files/legal_informations/';

    public static function tableName()
    {
        return 'legal_informations';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['file'], 'file'],
            [['created_at', 'updated_at'], 'safe'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'file' => 'Файл',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'sort' => 'Сортировка',
        ];
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = LegalInformation::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public function getFile(){
        return '/backend/web/'.$this->path.''.$this->file;
    }

    public static function getAll(){
        return self::find()->orderBy('sort', 'ASC')->all();
    }
}
