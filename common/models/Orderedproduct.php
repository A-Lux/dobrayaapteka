<?php
namespace common\models;

use Yii;
use common\models\Products;

/**
 * This is the model class for table "Orderedproduct".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $count
 * @property int $isGift
 */


class Orderedproduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orderedproduct';
    }

    public function rules()
    {
        return [
            [['order_id', 'product_id', 'count','isGift'], 'required'],
            [['order_id', 'product_id', 'count','isGift'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
        ];
    }
	
	public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public static function createNew($order_id, $product_id, $count, $status){

        $orderedProducts = new self();
        $orderedProducts->product_id = $product_id;
        $orderedProducts->count = $count;
        $orderedProducts->order_id = $order_id;
        $orderedProducts->isGift = $status;
        return  $orderedProducts->save(false);

    }
}
