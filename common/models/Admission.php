<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Admission".
 *
 * @property int $id
 * @property int $user_id
 * @property int $menu
 * @property int $mainsub
 * @property int $banner
 * @property int $advantages
 * @property int $country
 * @property int $filial
 * @property int $catalog
 * @property int $products
 * @property int $promocode
 * @property int $geocoords
 * @property int $gift
 * @property int $discount
 * @property int $free_delivery
 * @property int $trigger_birthday
 * @property int $trigger_holiday
 * @property int $trigger_tovar_month
 * @property int $trigger_remind_basket
 * @property int $orders
 * @property int $contact
 * @property int $logo
 * @property int $text
 * @property int $review
 * @property int $email
 * @property int $messages
 * @property int $news
 * @property int $clients
 * @property int $sliders
 * @property int $search_banner;

 */


class Admission extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'admission';
    }

    public function rules()
    {
        return [
            [['user_id', 'menu', 'mainsub', 'banner', 'advantages', 'filial', 'catalog', 'products', 'sliders', 'search_banner',
                'country', 'geocoords', 'promocode', 'gift', 'discount', 'free_delivery', 'trigger_birthday',
                'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'sms_notification', 'promotional_products',
                'orders', 'contact', 'logo', 'text', 'review', 'email', 'messages', 'news', 'clients', 'legal_information'], 'required'],
            [['user_id', 'menu', 'mainsub', 'banner', 'advantages', 'filial', 'catalog', 'products', 'sliders',
                'country', 'geocoords', 'promocode', 'gift', 'discount', 'free_delivery', 'promotional_products', 'trigger_birthday',
                'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'sms_notification', 'search_banner',
                'orders', 'contact', 'logo', 'text', 'review', 'email', 'messages', 'news', 'clients', 'legal_information'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'menu' => 'Menu',
            'mainsub' => 'Mainsub',
            'banner' => 'Banner',
            'advantages' => 'Advantages',
            'filial' => 'Filial',
            'catalog' => 'Catalog',
            'products' => 'Products',
            'country' => 'Country',
            'geocoords' => 'Geocoords',
            'promocode' => 'Promocode',
            'gift' => 'Gift',
            'discount' => 'Discount',
            'free_delivery' => 'Free Delivery',
            'promotional_products'  => 'Promotional Products',
            'trigger_birthday' => 'Trigger Birthday',
            'trigger_holiday' => 'Trigger Holiday',
            'trigger_tovar_month' => 'Trigger Tovar Month',
            'trigger_remind_basket' => 'Trigger Remind Basket',
            'orders' => 'Orders',
            'contact' => 'Contact',
            'logo' => 'Logo',
            'text' => 'Текст',
            'review' => 'Review',
            'email' => 'Email',
            'messages'  => 'Messages',
            'sms_notification' => 'Sms Notification',
            'news'  => 'News',
            'clients'   => 'Clients',
            'legal_information' => 'Legal Information',
            'sliders' => 'Sliders',
            'search_banner' => 'Search banner',
        ];
    }
}
