<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "News".
 *
 * @property int $id
 * @property int $sort
 * @property string $name
 * @property string $image
 * @property string $short_description
 * @property string $full_description
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 * @property string $created_at
 * @property string $updated_at
 * @property string $url
 */

class News extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/news/';

    public static function tableName()
    {
        return 'news';
    }

    public function rules()
    {
        return [
            [['name', 'short_description', 'full_description', 'metaName'], 'required'],
            [['short_description', 'full_description', 'metaDesc', 'metaKey'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['sort'], 'integer'],
            [['name', 'metaName', 'url'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'sort' => 'Сортировка',
            'url' => 'Ссылка'
        ];
    }


    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public function getImageUrl(){
        return '@backend/web/'.$this->path;
    }

    public static function getAll(){
        return self::find()->orderBy('sort ASC')->all();
    }

    public function generateCyrillicToLatin(){

        // convert letters start
        $cyr = [
            'ый','а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
            ',','.',' ','/','(',')','|','_'
        ];
        $lat = [
            'iy','a','b','v','g','d','e','e','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','shch','','y','','e','yu','ya',
            'a','b','v','g','d','e','e','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','shch','','y','','e','yu','ya',
            '-','-','-','-','-','-','-','-'
        ];
        $text = str_replace($cyr, $lat, $this->name);
        // convert letters end


        // convert more hyphen start
        $cyr = ['------', '-----', '----', '---', '--'];
        $lat = ['-', '-', '-', '-', '-',];

        $text = str_replace($cyr, $lat, $text);
        // convert more hyphen end


        // delete start && last hyphen
        if(substr($text, -1) == '-') {
            $text = substr($text, 0, -1);
        }

        if(substr($text, 0, 1) == '-') {
            $text = substr($text, 1);
        }
        // delete start && last hyphen

        return strtolower(preg_replace('/[^a-zA-Z0-9-]/','', $text));
    }

}
