<?php
namespace common\models;

use Yii;

class FilialProduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'filial_product';
    }

    public function rules()
    {
        return [
            [['filial_id', 'product_id', 'amount'], 'required'],
            [['filial_id', 'product_id', 'amount'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filial_id' => 'Filial ID',
            'product_id' => 'Product ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
        ];
    }


    public static function getSomeByOrder($products){

        $ids = '';
        foreach ($products as $v){
            $ids .= $v->product_id. ',';
        }
        $ids = rtrim($ids, ",");
        return self::find()
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere('filial_product.product_id in ('.$ids.')')
            ->all();
    }


    public static function getByProduct($product_id){
        return self::find()
            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
            ->where('filial.status = 2')
            ->andWhere(['filial_product.product_id' => $product_id])
            ->orderBy('filial_product.filial_id')
            ->all();
    }



    public function getFilial(){
        return $this->hasOne(Filial::className(), ['id' => 'filial_id']);
    }

    public function getFilialName(){
        return $this->filial ? $this->filial->address : "Не найдено";
    }

    public function getFilialNumber(){
        return $this->filial ? $this->filial->name : "Не найдено";
    }




}
