<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Analogy".
 *
 * @property int $id
 * @property int $product1
 * @property int $product2
 * @property string $ware_id_1
 * @property string $ware_id_2
 */

class Analogy extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'analogy';
    }

    public function rules()
    {
        return [
            [['ware_id_1', 'ware_id_2'], 'required'],
            [['product1', 'product2'], 'integer'],
            [['ware_id_1', 'ware_id_2'], 'string','max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product1' => 'Product1',
            'product2' => 'Product2',
            'ware_id_1' => 'ware_id_1',
            'ware_id_2' => 'ware_id_2'
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'product2']);
    }


    public static function getProducts($id){

        $product = Analogy::findAll(['product1' => $id]);
        $res = array();
        foreach ($product as $v) $res[$v->id] = $v->id;
        return $res;
    }

    public static function getProductsBySql($Product){

        $analogy = self::find()->where('product1 = '.$Product->id. ' OR product2 = '.$Product->id)->all();
        $in = [];
        foreach ($analogy as $v){
            if($Product->id == $v->product1)
                if($v->product2 != null) {
                    $in[] = $v->product2;
                }
            else
                if($v->product1 != null) {
                    $in[] = $v->product1;
                }
        }

        if($in) {
            $sql = 'products.id in ('.implode(',', $in).')';
            $model =  Products::getByQuery($sql);
        }else{
            $model = null;
        }

        return $model;
    }

    public function createNew($ware_id_1, $ware_id_2){

        $this->ware_id_1 = $ware_id_1;
        $this->ware_id_2 = $ware_id_2;
        return $this->save();
    }

}
