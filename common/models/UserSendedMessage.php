<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $year
 * @property string $month
 * @property string $day
 * @property string $user_id
 * @property string $type
 */

class UserSendedMessage extends \yii\db\ActiveRecord
{

    const isHoliday = 2;
    const isBirthday = 1;
    public static function tableName()
    {
        return 'user_sended_message';
    }

    public function rules()
    {
        return [
            [['year', 'month', 'day', 'user_id', 'type'], 'required'],
            [['year', 'month', 'day', 'user_id', 'type'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Year',
            'month' => 'Month',
            'day' => 'Day',
            'user_id' => 'User ID',
            'type' => 'Type',
        ];
    }

    public static function getAllHolidaySendMessages(){
        return UserSendedMessage::find()->where('type='.self::isHoliday)->all();
    }

    public static function getAllBirthdaySendMessages(){
        return UserSendedMessage::find()->where('type='.self::isBirthday)->all();
    }

    public function saveBirthdayMessage($user_id, $year, $month, $day){
        $this->user_id = $user_id;
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
        $this->type = self::isBirthday;
        return $this->save(false);
    }

    public function saveHolidayMessage($user_id, $year, $month, $day){
        $this->user_id = $user_id;
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
        $this->type = self::isHoliday;
        return $this->save(false);
    }

}
