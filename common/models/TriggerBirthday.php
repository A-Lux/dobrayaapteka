<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "TriggerBirthday".
 * @property int $id
 * @property string $content
 */

class TriggerBirthday extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'trigger_birthday';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Текст',
        ];
    }

    public static function getContent(){
        return TriggerBirthday::find()->one();
    }
}
